﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoginController : MonoBehaviour
{
    public string username = "";
    public int stId = 0;
    public string hasFinished = "";
    public int totalSeconds = 0;
    public string birth = "";

    public Text UserName;
    public Text Birth;
    public GameObject LoginPanel;
    public ChoiceTime choiceTimeController;

    public Text btn_start_text;

    //public Text Sex;
    //public Text Birth;

    public Text Hint;

    public WWWController myWWW;

    // Start is called before the first frame update
    void Start()
    {
        StayorNot();
        btn_start_text.text = "登錄";
        myWWW = this.GetComponent<WWWController>();
        Hint.gameObject.SetActive(false);
    }


    // Update is called once per frame
    void Update()
    {

    }

    public bool GetUserName() {
        username = UserName.text;
        if (username.Trim() == "")
        {
            Hint.gameObject.SetActive(true);
            Hint.text = "名字不能為空！";
            return false;
        }
        else {
            Hint.gameObject.SetActive(false);
        }
        return true;
    }

    /// <summary>
    /// 判斷本地有沒有保存的用戶id
    /// 如果沒有，則在當前頁面，如果有，則直接進入到下個界面
    /// </summary>
    /// 
    public void StayorNot() {
        string playerId = PlayerPrefs.GetString("playerId","");
        if (playerId != "") {
            SceneManager.LoadScene("Main");
        }
    }

    public void Loginin() {

        if (choiceTimeController.isShowChoiceTime)
        {
            choiceTimeController.StartChoiceTime();
        }
        //獲取出生日期
        if (GetBirth() && GetUserName()) {
            myWWW.FindUser(username, birth);
            //存在名字，拿到已完成的東西，並在prefeb保存下來
            btn_start_text.text = "登錄中...";
        }


    }


    public void FindNameSuccess(user user) {
        PlayerPrefs.SetInt("scolarId", user.id);
        PlayerPrefs.SetString("hasFinished", user.hasFinished);
        PlayerPrefs.SetInt("totalSeconds", user.totalSeconds);
        Debug.Log("user.hasFinished.ToString() == :" + (user.hasFinished.ToString() == ""));
        if (user.hasFinished.ToString() == "") {
            SceneManager.LoadScene("Video");
            return;
        }
        Debug.Log("登錄到主頁面中");
        SceneManager.LoadScene("Main");
    }

    public void FindNamerFail()
    {
        Hint.gameObject.SetActive(true);
        Hint.text = "請檢查你的姓名，出生日期是否正確。如有需要，請聯繫研究助理。";
        btn_start_text.text = "登錄";
    }

    public void ConnectedFail()
    {
        Hint.gameObject.SetActive(true);
        Hint.text = "網絡有問題，請檢查網絡。";
    }

    public bool GetBirth()
    {
        birth = Birth.text;
        if (birth.Contains("yy") || birth.Contains("mm") || birth.Contains("dd"))
        {
            choiceTimeController.StartChoiceTime();
            Hint.gameObject.SetActive(true);
            Hint.text = "出生日期不能為空！";
            return false;
        }
        else {
            Hint.gameObject.SetActive(false);
        }

        return true;
    }
}
