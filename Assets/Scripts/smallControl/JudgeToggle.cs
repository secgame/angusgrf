﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JudgeToggle : MonoBehaviour
{
    GameObject rightMark;
    GameObject wrongMark;
    int currentIndex;

    // Start is called before the first frame update
    void Start()
    {
        rightMark = transform.GetChild(0).gameObject;
        wrongMark = transform.GetChild(1).gameObject;
        rightMark.SetActive(false);
        wrongMark.SetActive(false);
        currentIndex = -1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ToggleChoice() {
        if (currentIndex == 0)
        {
            rightMark.SetActive(true);
            wrongMark.SetActive(false);
            currentIndex = 1;
        }
        else {
            rightMark.SetActive(false);
            wrongMark.SetActive(true);
            currentIndex = 0;
        }


    }
}
