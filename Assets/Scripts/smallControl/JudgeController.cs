﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JudgeController : MonoBehaviour
{
    public int pngCount;
    public Text Hint;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public string SaveAns() {
        bool[] ansStrArr = new bool[pngCount];
        int i = 0;
        Debug.Log("length:"+ pngCount);
        foreach (Transform child in transform) {
            if (child.gameObject.activeSelf) {
                if (child.GetChild(0).gameObject.activeSelf)
                {
                    ansStrArr[i] = true;
                    i++;
                }
                if (child.GetChild(1).gameObject.activeSelf)
                {
                    ansStrArr[i] = false;
                    i++;
                }          
            }
        }
        if (i < pngCount - 1) {
            return null;
        }

        string ansString = string.Join(",", ansStrArr);


        return ansString;
    }

    public void submit() {
        string ansString = SaveAns();
        if (ansString == null) {
            Debug.Log("have not finished");
            Hint.text = "需要全部回答完才能提交答案";
            return;
        }
        Debug.Log("judgeTrueorFalse:" + ansString);
        if (!Controller.instance.QuestionList[Controller.instance.CurrentQuestion].IsTrial) { 
            Controller.instance.myWWW.SaveToDatabase(Controller.instance.userId, int.Parse(Controller.instance.QuestionList[Controller.instance.CurrentQuestion].Index), ansString, Controller.instance.startTimeStamp, Controller.GetTimeStamp());
            Controller.instance.UpdateQuestionNum();
        }
        Controller.instance.StartPostQuestion();
 
    }

    public void PrepareGame(string wordsPngs) {
        Debug.Log("wordsPngs:" + wordsPngs);
        string[] wordsPngArr = wordsPngs.Split(',');
        pngCount = 0;
        for (int i =0;i<wordsPngArr.Length;i++) {
            if (wordsPngArr[i] !="") 
                pngCount++;
        }
        foreach (Transform child in transform) {
            child.gameObject.SetActive(true);
            foreach (Transform tell in child) {
                tell.gameObject.SetActive(false);
            }
            child.gameObject.SetActive(false);

        }
        Debug.Log("pngCount.length:"+ pngCount);
        Hint.text = "点击图片可以切换判断";

        for (int i = 0; i < pngCount; i++) {
            if (i > 23)
                return;
            transform.GetChild(i).gameObject.SetActive(true);
            Debug.Log("Images/" + wordsPngArr[i].Split('.')[0]);
            transform.GetChild(i).gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("Images/" + wordsPngArr[i].Split('.')[0]);
        }
    }
}
