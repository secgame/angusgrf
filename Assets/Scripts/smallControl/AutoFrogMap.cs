﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AutoFrogMap : MonoBehaviour
{
    public GameObject AllInstruction;
    public GameObject SubInstruction;
    public GameObject ReadyInstruction;
    public Text Hint;
    public GameObject ItemsParent;
    public Image[] Leafs;
    public frogSetting[] FrogsSetting;
    public Image Frog;
    public Button Continue;
    public Button btn_BeginAns;

    //音頻
    public AudioClip[] leafAudio; //ok
    public AudioClip[] LeafAudio_reversed; //ok
    public AudioClip[] excerciseAudio; //ok
    public AudioClip[] feedbackAudio;
    public AudioClip[] pressBeginAudio;
    public AudioClip[] AllInstructionAudio; //ok
 

    Vector2[] LeafsPos;
    Vector2 FrogPos;
    int[] leafIndexs;
    int[] AnsLeafIndexs;
    frogSetting currentFrogSetting;
    public int currentSettingIndex;//大题目的序号
    public int currentLevel;
    public int currentQue;//小题目的序号
    public int correctNum;
    bool EnableAnswered;//允许回答问题
    int leafMax;
    public bool ordered;

    public string startTimeStamp;
    public string endTimeStamp;

    // Start is called before the first frame update
    void Start()
    {
        
        


        //id = 0;isTrail = 1;level = 1; MaxNum = 0; passNum =0;
        //id = 1;isTrail = 1;level = 2; MaxNum = 0; passNum =0;
        //id = 2;isTrail = 0;level = 1; MaxNum = 6; passNum =4;
        //id = 3;isTrail = 0;level = 2; MaxNum = 6; passNum =4;
        //id = 4;isTrail = 0;level = 3; MaxNum = 6; passNum =4;
        //id = 5;isTrail = 0;level = 4; MaxNum = 6; passNum =4;
        //id = 6;isTrail = 0;level = 5; MaxNum = 6; passNum =4;
        //id = 7;isTrail = 0;level = 6; MaxNum = 6; passNum =4;
        //id = 8;isTrail = 0;level = 7; MaxNum = 6; passNum =4;
        //id = 9;isTrail = 0;level = 8; MaxNum = 6; passNum =4;
        //id = 10;isTrail = 0;level = 9; MaxNum = 6; passNum =4;
    }

    void ResetFrog() {
        //Frog.gameObject.SetActive(true);
        Frog.transform.localPosition = FrogPos;
    }

    void ResetLeafNum() {
        foreach (Image img in Leafs) {
            img.transform.GetChild(0).GetComponent<Text>().text = "";
        }
    }

    // Update is called once per frame
    void Update()
    {


    }

    private void Gameover()
    {
        string str = Controller.instance.hasFinished;
        if (str == "")
        {
            str = currentFrogSetting.TestIndex.ToString();
        }
        else
        {
            str = str + "," + currentFrogSetting.TestIndex.ToString();
        }

        PlayerPrefs.SetString("hasFinished", str);
        Controller.instance.hasFinished = str;
        //保存到數據庫
        StartCoroutine(Controller.instance.myWWW.UpdateUser());
        Controller.instance.GameOver();
    }

    /// <summary>
    /// 準備題
    /// </summary>
    public void GoToNextQue() {

        if (!currentFrogSetting.IsTrial)
        {
            //如果題目已經達到了最後一條，但是正確率小於死->結束
            if (currentQue > currentFrogSetting.MaxNum-1 && correctNum < currentFrogSetting.PassNum)
            {
                Debug.Log("Finished1");
                Debug.Log("currentQue:" + currentQue + "," + "correctNum:" + correctNum);

                Gameover();
            }//如果當前題目序號未達到最後一道題目並且正確題目以達到四道題目，則跳到下一阶级；
            else if (currentQue <= currentFrogSetting.MaxNum && correctNum >= currentFrogSetting.PassNum)
            {

                Debug.Log("currentQue:" + currentQue + "," + "correctNum:" + correctNum);             
                panelIndex = 0;
                ContinueOnClick();
            }
            else //如果其他情況，則顯示下一道題
            {
                Debug.Log("currentQue:" + currentQue + "," + "correctNum:" + correctNum);              
                //下一题
                currentQue++;

                if (Hint.text == "准备好了吗?")
                {
                    ShowQuestion();
                }
                else {
                    Hint.text = "准备好了吗?";
                    //顯示過度
                    ItemsParent.SetActive(false);
                    ReadyInstruction.SetActive(true);
                    Invoke("ShowQuestion", 2f);
                }
                
            }
        }
        else {
            if (currentQue == 0)
            {
                //下一题
                currentQue++;
                ShowQuestion();
            }
            else {
                panelIndex = 0;
                ContinueOnClick();
            }
        }

            
        }


    /// <summary>
    /// 準備題庫
    /// </summary>
     public void GoToNextSetting() {
            if (currentSettingIndex < FrogsSetting.Length)
            {
                currentFrogSetting = FrogsSetting[currentSettingIndex];

                currentLevel = currentFrogSetting.Level;
                currentQue = 0;
                correctNum = 0;

        }
            else {
                Debug.Log("Finished");
            return;
            }
            currentSettingIndex++;
            

    }

    int clickIndex;
    public void SavePosAns(int leafNo) {

        if (EnableAnswered) {
           
            AnsLeafIndexs[clickIndex] = leafNo;
            //Leafs[leafNo].transform.GetChild(0).GetComponent<Text>().text  = (clickIndex+1).ToString();
            clickIndex++;
            Hint.text =  "答题中... 还需跳"+ (currentLevel - clickIndex)+ "片荷叶";
            if (clickIndex >= AnsLeafIndexs.Length)
            {
                EnableAnswered = false;
                clickIndex = 0;
                //进行判断
                Invoke("JudgeAns", 1f);
            }
        }

    }

    public bool compareArr(int[] arr1, int[] arr2, bool isOrdered)
    {
        if (!isOrdered)
        {
            int n;
            //int x = array.Length;
            for (int i = 0; i < arr2.Length / 2; i++)
            {
                n = arr2[i];
                arr2[i] = arr2[arr2.Length - i - 1];
                arr2[arr2.Length - i - 1] = n;


            }
        }
        //var q = from a in arr1 join b in arr2 on a equals b select a;
        bool flag;

        if (string.Join("-", arr1).Equals( string.Join("-", arr2)))
        {
            flag = true;
        }
        else
            flag = false;

        //bool flag = arr1.Length == arr2.Length && q.Count() == arr1.Length;
        Debug.Log("flag:"+flag);

        return flag;//内容相同返回true,反之返回false。

    }

    public void JudgeAns() {
        //ResetLeafNum();
        if (currentFrogSetting.IsTrial)
        {          
            if (compareArr(AnsLeafIndexs,leafIndexs,ordered))
            {
                Hint.text = "回答正确，做得好！";
                Controller.instance.PlaySound(feedbackAudio[0]);              
                //panelIndex = 1;
                replay = false;
                Invoke("GoToNextQue", 2f);
            }
            else
            {
                Hint.text = "回答错误，正确答案应该是";
                Controller.instance.PlaySound(feedbackAudio[1]);
                replay = true;
                ResetFrog();
                currentFrame = 0;
                Invoke("PlayAFrame", 2f);
            }
        }
        else {
            /*for (int i = 0; i < AnsLeafIndexs.Length; i++) {
                Debug.Log("A1:" + AnsLeafIndexs[i]);
            }

            for (int i = 0; i < leafIndexs.Length; i++)
            {
                Debug.Log("L1:" + leafIndexs[i]);
            }*/

            if (compareArr(AnsLeafIndexs, leafIndexs, ordered)) {
                //保存數據
                endTimeStamp = Controller.GetTimeStamp();
               /* if (!ordered)
                {
                    int n;
                    //int x = array.Length;
                    for (int i = 0; i < leafIndexs.Length / 2; i++)
                    {
                        n = leafIndexs[i];
                        leafIndexs[i] = leafIndexs[leafIndexs.Length - i - 1];
                        leafIndexs[leafIndexs.Length - i - 1] = n;


                    }
                }*/

                
                correctNum++;

            }
            //无论是否回答正确都要保留答案
                string ansSring = "{ 'correctAns':" + string.Join("-", leafIndexs) + ",'userAns':" + string.Join("-", AnsLeafIndexs)+"}";
                //保存到数据库
                submit(string.Join("-", leafIndexs), string.Join("-", AnsLeafIndexs));
                Debug.Log("ansSring:"+ ansSring);

            replay = false;     
            GoToNextQue();
            ResetFrog();
            //Invoke("ShowPanel", 1.5f);
        }
        
    }

    public void submit(string correctAns, string userAns) {
        Controller.instance.myWWW.SaveToDatabase(Controller.instance.userId, int.Parse(currentFrogSetting.Index), "correntAns:" + correctAns + "; userAns:" + userAns + ";questionNo:" + currentQue + ";FrogNum:" + currentFrogSetting.Level, startTimeStamp, endTimeStamp);

    }

    public void ShowQuestion() {
        currentFrame = 0;
        panelIndex = 3;
        ShowPanel();
        leafIndexs = new int[currentLevel];
        if (currentLevel != leafMax)
        {
            System.Random gen = new System.Random();
            for (int i = 0; i < leafIndexs.Length; i++)
            {
                //随机生成位置
                int geNum = -1;
                do
                {
                    geNum = gen.Next(0, 9);

                } while (leafIndexs.Contains(geNum));

                leafIndexs[i] = geNum;

            }
        }
        else {
            for (int i = 0; i < leafMax; i++) {
                leafIndexs[i] = i;
            }

            leafIndexs = RandomArr(leafIndexs);
        }

        
        startTimeStamp = Controller.GetTimeStamp();
        Invoke("PlayAFrame", 0.5f);
    }

    //打亂數組
    public int[] RandomArr(int[] arr)
    {
        System.Random gen = new System.Random();//创建随机类对象，定义引用变量为r
        for (int i = 0; i < arr.Length; i++)
        {
            int index = gen.Next(arr.Length);//随机获得0（包括0）到arr.Length（不包括arr.Length）的索引
                                           //Console.WriteLine("index={0}", index);//查看index的值
            int temp = arr[i];  //当前元素和随机元素交换位置
            arr[i] = arr[index];
            arr[index] = temp;
        }

        return arr;
    }

    //动画当中的一帧
    int currentFrame;
    bool replay;
    public void PlayAFrame() {
        Frog.gameObject.SetActive(true);
        if (currentFrame > leafIndexs.Length - 1) {
            return;
        }

        Frog.transform.localPosition = LeafsPos[leafIndexs[currentFrame]];
        currentFrame++;
        if (currentFrame < leafIndexs.Length)
        {
            Invoke("PlayAFrame", 1.5f);
        }
        else {
            if (!replay)
            {
                //允许答题
                
                AnsLeafIndexs = new int[leafIndexs.Length];
                clickIndex = 0;
                panelIndex = 4;
                Invoke("ShowPanel", 1.5f);
            }
            else {
                Invoke("ResetFrog", 1.5f);
                Invoke("GoToNextQue", 3f);
                replay = false;
            }
            
        }

        

    }

    private IEnumerator AudioPlayFinished(float time)
    {
        Continue.interactable = false;
        yield return new WaitForSeconds(time);
        Continue.interactable = true;


    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="panelIndex">0->總介紹 1->部分介紹，2->顯示【準備好了嗎】,3->青蛙跳荷葉，4->作出反應，5-> 反饋</param>
    int panelIndex = 0;
    void ShowPanel() {
        
        switch (panelIndex) {
            case 0:
                ItemsParent.SetActive(false);
                AllInstruction.SetActive(true);
                
                if (ordered)
                {//按照順序
                    AllInstruction.GetComponent<Text>().text = "我们来玩一个“青蛙在哪里？”的游戏！\n 我们先来学习如何玩这个游戏。等一下屏幕上会出现一张有9片荷叶的图片,青蛙会跳上这些荷叶，跳到另一个地方去。\n 你需要记住青蛙通过这些荷叶的顺序。\n现在，我们先来练习一下吧。";
                    Controller.instance.PlaySound(AllInstructionAudio[0]);
                    StartCoroutine(AudioPlayFinished(AllInstructionAudio[0].length));
                    
                }
                else {
                    AllInstruction.GetComponent<Text>().text = "在上一次的游戏中，你很好的记住了青蛙的跳跃路线。现在让我们来试试更有难度的。你要记住青蛙跳跃的顺序，但是倒着一个个点出来。";
                    Controller.instance.PlaySound(AllInstructionAudio[1]);
                    StartCoroutine(AudioPlayFinished(AllInstructionAudio[1].length));
                }

                SubInstruction.SetActive(false);
                ReadyInstruction.SetActive(false);
                Hint.gameObject.SetActive(false);
                Frog.gameObject.SetActive(false);
                Continue.gameObject.SetActive(true);
                Continue.transform.GetChild(0).gameObject.GetComponent<Text>().text = "继续"; 
                break;
            case 1:
                ItemsParent.SetActive(false);
                AllInstruction.SetActive(false);
                SubInstruction.SetActive(true);

                ReadyInstruction.SetActive(false);
                Hint.gameObject.SetActive(false);
                Frog.gameObject.SetActive(false);
                Continue.gameObject.SetActive(true);
                Continue.transform.GetChild(0).gameObject.GetComponent<Text>().text = "继续";
                break;
            case 2:
                ItemsParent.SetActive(false);
                AllInstruction.SetActive(false);
                SubInstruction.SetActive(false);
                ReadyInstruction.SetActive(true);
                Hint.gameObject.SetActive(true);
                Frog.gameObject.SetActive(false);
                ResetFrog();
                Continue.gameObject.SetActive(false);
                Hint.text = "准备好了吗?";
                StartCoroutine(HideReady());
                break;
            case 3:
                ItemsParent.SetActive(true);
                AllInstruction.SetActive(false);
                SubInstruction.SetActive(false);
                ReadyInstruction.SetActive(false);
                Hint.gameObject.SetActive(true);
                Frog.gameObject.SetActive(true);
                Continue.gameObject.SetActive(false);
                Hint.text = "播放动画中";
                break;
            case 4:
                ItemsParent.SetActive(true);
                AllInstruction.SetActive(false);
                SubInstruction.SetActive(false);
                ReadyInstruction.SetActive(false);
                Hint.gameObject.SetActive(true);
                ResetFrog();
                Frog.gameObject.SetActive(true);
                Continue.gameObject.SetActive(false);
                Hint.text = "点击【开始答题】进行答题！";
                btn_BeginAns.gameObject.SetActive(true);

                    if (ordered)
                    {
                        Controller.instance.PlaySound(pressBeginAudio[0]);
                    }
                    else
                    {
                        Controller.instance.PlaySound(pressBeginAudio[1]);
                    }
                
                

                break;
            case 5:
                ItemsParent.SetActive(true);
                AllInstruction.SetActive(false);
                SubInstruction.SetActive(false);
                ReadyInstruction.SetActive(false);
                Frog.gameObject.SetActive(true);
                Hint.gameObject.SetActive(true);                
                Continue.gameObject.SetActive(true);
                Frog.gameObject.SetActive(true);
                Hint.text = "正确答案播放中";
                break;
        }

    }

    /// <summary>
    /// 按鈕點擊，進入總介紹還是部分介紹
    /// </summary>
    /// <param name="state">state = 0->subInstruction; state = 1 -> goTotrial</param>
    public void ContinueOnClick() {
        Debug.Log("panelIndex:"+ panelIndex);
        Controller.instance.AudioPlayer.Stop();
        switch (panelIndex) {
            case 0:
                //準備題庫
                GoToNextSetting();
                if (currentFrogSetting.IsTrial)
                {
                    SubInstruction.GetComponent<Text>().text = "现在，我们来试试" + currentFrogSetting.Level + "片荷叶的情况。\n\n准备好了吗？";
                    Controller.instance.PlaySound(excerciseAudio[currentFrogSetting.Level - 1]);
                    StartCoroutine(AudioPlayFinished(excerciseAudio[currentFrogSetting.Level - 1].length));
                }
                else {
                    
                    if (ordered)
                    {
                        SubInstruction.GetComponent<Text>().text = "现在，你会看到青蛙跳上" + currentFrogSetting.Level + "片荷叶。等到青蛙彻底消失之后，你依次指出来给我看刚才青蛙出现的位置。\n\n准备好了吗？";
                        Controller.instance.PlaySound(leafAudio[currentFrogSetting.Level - 1]);
                        StartCoroutine(AudioPlayFinished(leafAudio[currentFrogSetting.Level - 1].length));
                    }
                    else {

                        SubInstruction.GetComponent<Text>().text = "现在，你会看到青蛙跳上" + currentFrogSetting.Level + "片。等到青蛙彻底消失之后，你按照倒过来的顺序指出来给我看刚才青蛙出现的位置。\n\n准备好了吗？";
                        Controller.instance.PlaySound(LeafAudio_reversed[currentFrogSetting.Level - 1]);
                        StartCoroutine(AudioPlayFinished(LeafAudio_reversed[currentFrogSetting.Level - 1].length));
                    }

                }
                panelIndex = 1;
                ShowPanel();              
                break;
            case 1:               
                panelIndex = 2;
                ShowPanel();
                break;
        }
    }

    public void Btn_BeginAnsClick() {
        Controller.instance.AudioPlayer.Stop();
        EnableAnswered = true;
        Hint.text = "答题中... 还需跳"+ currentLevel + "片荷叶";
        btn_BeginAns.gameObject.SetActive(false);
    }

    public void SetFrogSettings(frogSetting[] settings,bool isOrdered)
    {
        FrogsSetting = settings;
        ordered = isOrdered;
        Debug.Log("Isorded:"+ordered);
        StartCoroutine(PrepareGame());
    }

    IEnumerator PrepareGame()
    {
        
        currentLevel = 0;
        currentQue = 0;
        correctNum = 0;
        currentSettingIndex = 0;
        leafMax = 9;
        yield return new WaitForSeconds(0.5f);
        panelIndex = 0;
        ShowPanel();
        ResetLeafNum();
        btn_BeginAns.gameObject.SetActive(false);

        LeafsPos = new Vector2[9];
        FrogPos = new Vector2();

        int tmp = 0;
        foreach (Image img in Leafs) {
            LeafsPos[tmp] = img.transform.localPosition;
            tmp++;
        }
        FrogPos = Frog.transform.localPosition;
    }



    IEnumerator HideReady()
    {       
        //播放題
        replay = false;     
        yield return new WaitForSeconds(2);
        //準備題
        //panelIndex = 3;
        //ShowPanel();
        GoToNextQue();
        
        
    }




}
