﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToTController : MonoBehaviour
{
    int Statue = 0; 
    //0->總介紹, 1->聽動物的聲音，2-> 練習前的介紹，3-> 進入練習，4-> 正式測試的介紹,5->總介紹，只是說錄音變了
    public Image ins_img;
    public Button btn_continue;
    public GameObject ControlPanel;
    public CanvasGroup ControlPanelHolder;
    public Button btn_Cow;
    public Button btn_Mouse;
    public Image ball;
    public Image smile;
    System.Random rd = new System.Random();

    public int questonAmount;
    public int questionIndex;
    public ArrayList tmpAnsCollection;

    int TestCount;
    bool isTrial;

    public AudioClip[] soundAudio; //0是mouse，1是cow
    public Sprite[] ins_Sprites;
    public AudioClip[] ins_audios;

    public int SOA;
    public List<int> animalSounds;
    public int Ans;

    string startTimeStamp;
    string endTimeStamp;

    public int currentlevel;
    //public float baseLevel;
    public int max;
    public int min;
    public int[] step;
    public int correctNum;
    public int cummulatedCorrectNum;

    // Start is called before the first frame update
    void Start()
    {
       
    }
   // RecordingHolderGroup.blocksRaycasts = false;

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame(bool init = true) {
        if (init)
        {
            Statue = 0;
        }
        else {
            Statue = 5;
        }
        tmpAnsCollection = new ArrayList();
         TestCount = 2;
        correctNum = 0;
        cummulatedCorrectNum = 0;
        step =new int[16] { 1, 2, 3, 4, 6, 10, 16, 25, 40, 63, 100, 158, 251, 398, 631, 1000 };     
        ShowStatue();
    }

    public void ShowStatue() {
        switch (Statue) {
            case 0: 
                ins_img.gameObject.SetActive(true);
                btn_continue.gameObject.SetActive(true);
                ControlPanel.gameObject.SetActive(false);
                ins_img.sprite = ins_Sprites[0];
                StartCoroutine(PlayInstructionSound(0));
                Controller.instance.QuestionNumText.gameObject.SetActive(false);
                break;
            case 5:
                ins_img.gameObject.SetActive(true);
                btn_continue.gameObject.SetActive(true);
                ControlPanel.gameObject.SetActive(false);
                ins_img.sprite = ins_Sprites[4];
                StartCoroutine(PlayInstructionSound(4));
                Controller.instance.QuestionNumText.gameObject.SetActive(false);
                break;
            case 1:
                ins_img.gameObject.SetActive(false);
                btn_continue.gameObject.SetActive(false);
                ControlPanel.gameObject.SetActive(true);
                ball.gameObject.SetActive(false);
                smile.gameObject.SetActive(false);
                btn_Cow.gameObject.SetActive(true);
                btn_Mouse.gameObject.SetActive(true);
                ControlPanelHolder.blocksRaycasts = false;
                Controller.instance.QuestionNumText.gameObject.SetActive(false);
                animalSounds.Clear();
                SOA = 200;              
                for (int i = 0; i < 3; i++) {
                    animalSounds.Add(0);
                }
                for (int i = 0; i < 3; i++)
                {
                    animalSounds.Add(1);
                }
                currentanimalIndex = 0;
                DisplaySounds();
                break;
            case 2:
                ins_img.gameObject.SetActive(true);
                btn_continue.gameObject.SetActive(true);
                ControlPanel.gameObject.SetActive(false);
                ins_img.sprite = ins_Sprites[1];
                StartCoroutine(PlayInstructionSound(1));
                Controller.instance.QuestionNumText.gameObject.SetActive(false);
                break;
            case 3:
                StopAllCoroutines();
                ins_img.gameObject.SetActive(false);
                btn_continue.gameObject.SetActive(false);
                ControlPanel.gameObject.SetActive(true);
                ControlPanelHolder.blocksRaycasts = false;
                currentanimalIndex = 0;
                currentlevel = 13;
                SOA = step[13];
                correctNum = 0;
                cummulatedCorrectNum = 0;
                ShowQuestion();
                tmpAnsCollection = new ArrayList();
                Controller.instance.QuestionNumText.gameObject.SetActive(true);
                break;
            case 4:
                StopAllCoroutines();
                ins_img.gameObject.SetActive(true);
                btn_continue.gameObject.SetActive(true);
                ControlPanel.gameObject.SetActive(false);
                if (TestCount == 2)
                {
                    ins_img.sprite = ins_Sprites[2];
                    StartCoroutine(PlayInstructionSound(2));
                }
                else {
                    ins_img.sprite = ins_Sprites[3];
                    StartCoroutine(PlayInstructionSound(3));
                }
                Controller.instance.QuestionNumText.gameObject.SetActive(false);
                break;
        }
    }

    public void ContinueClick() {
        switch (Statue) {
            case 0:
            case 5:
                Statue = 1;
                ShowStatue();
                break;
            case 2:
                isTrial = true;
                max = 13;
                min = 11;
                currentlevel = 13;
                questonAmount = 10;
                questionIndex = 1;
                Statue = 3;
                ShowStatue();
                break;
            case 4:
                isTrial = false;
                max = 15;
                min = 0;
                questonAmount = 67;
                currentlevel =13;
                questionIndex = 1;
                Statue = 3;
                ShowStatue();
                break;


        }
    }
    int currentanimalIndex = 0;
    public void DisplaySounds() {
        if (currentanimalIndex < animalSounds.Count)
        {
         StartCoroutine(PlayAnimalSound(animalSounds[currentanimalIndex]));
        }
        else {
            if (Statue == 1)
            {
                Statue = 2;
                ControlPanelHolder.blocksRaycasts = true;
                ShowStatue();
            }
            else {            
                ball.gameObject.SetActive(false);
                ControlPanelHolder.blocksRaycasts = true;
                startTimeStamp = Controller.GetTimeStamp();
            }
           
        }
    }

    IEnumerator PlayAnimalSound(int index) {
        if (currentanimalIndex == 0)
        {
            yield return new WaitForSeconds(0.02f);
        }
        else
        {
            yield return new WaitForSeconds(SOA * 0.001f);
        }
        ControlPanelHolder.blocksRaycasts = false;
        if(Statue==1)
            InvokeRepeating("showHide", 0.0375f, 2);
        Controller.instance.PlaySound(soundAudio[index]);
        StartCoroutine(AudioPlayFinished(75*0.001f));
        currentanimalIndex++;
    }

    IEnumerator PlayInstructionSound(int index) {
        Controller.instance.PlaySound(ins_audios[index]);
        btn_continue.interactable = false;
        yield return new WaitForSeconds(ins_audios[index].length);
        btn_continue.interactable = true;
    }

    void showHide()
    {
        GameObject btn;
        Debug.Log("currentanimalIndex:" + (currentanimalIndex-1));
        if (animalSounds[currentanimalIndex-1] == 0)
        {
            btn = btn_Mouse.gameObject;
        }
        else {
            btn = btn_Cow.gameObject ;
        }
        if (btn.activeSelf == false)
        {
            btn.SetActive(true);
        }
        else
        {
            btn.SetActive(false);
        }

    }

    private IEnumerator AudioPlayFinished(float time)
    {   
        
        yield return new WaitForSeconds(time);
        Controller.instance.StopAudioPlayer();
        DisplaySounds();
    }

    public void ShowQuestion() {
        CancelInvoke();
        btn_Mouse.gameObject.SetActive(true);
        btn_Cow.gameObject.SetActive(true);

        if ((questonAmount - questionIndex) < 0)
        {
            if (Statue == 1)
            {
                Statue = 2;
                ShowStatue();
                Invoke("ShowStatue", 0.3f);
            }
            else
            {
                if (!isTrial)
                {
                    TestCount--;
                    //保存到數據庫
                    StartCoroutine(SaveToDataBase());
                    if (TestCount <= 0)
                    {
                        Gameover();
                        return;
                        //更新題目列表
                    }
                    
                }
                else {
                    //重來一次測試
                    if (correctNum <7) {
                        StartGame(false);
                        return;
                    }
                    
                }

                Statue = 4;
                Invoke("ShowStatue", 0.3f);
            }
            return;
        }
        else {
            currentanimalIndex = 0;
            animalSounds.Clear();
            int first = rd.Next(0, 2);
            int second = rd.Next(0, 2);
            //task1: 增加題目進度顯示
            if (isTrial)
            {
                Controller.instance.QuestionNumText.text = "答題進度(練習): " + questionIndex + "/" + questonAmount;
            }
            else
            {
                Controller.instance.QuestionNumText.text = "答題進度: " + questionIndex + "/" + questonAmount;
            }
            Ans = first;
            animalSounds.Add(first);
            animalSounds.Add(second);
            //SOA = step[currentlevel];
            ball.gameObject.SetActive(true);
            ControlPanelHolder.blocksRaycasts = false;
            DisplaySounds();
        }
        
    }

    private void Gameover()
    {
        string str = Controller.instance.hasFinished;
        if (str == "")
        {
            str = "10";
        }
        else
        {
            str = str + ",10";
        }

        PlayerPrefs.SetString("hasFinished", str);
        Controller.instance.hasFinished = str;
        //保存到數據庫
        StartCoroutine(Controller.instance.myWWW.UpdateUser());
        Controller.instance.GameOver();
    }

    int questionid_db;
    string correctAns;
    string userAns;
    string audioOrder;
    float currentSOA;
    bool correctorNot;
    public void JudgeQuestion(int ans) {
        questionid_db = 10000+ (3 - TestCount) * 100 + questionIndex;
        correctAns = Ans == 1 ? "Cow" : "Mouse";
        userAns = ans == 1 ? "Cow" : "Mouse";
        audioOrder = animalSounds[0] + "," + animalSounds[1];
        currentSOA = SOA;
        correctorNot = (ans == Ans);
        if (correctorNot) {
            correctNum++;
        }
        StartCoroutine(ShowSmile(correctorNot));
        questionIndex++;

    }

    IEnumerator ShowSmile(bool correct) {
        endTimeStamp = Controller.GetTimeStamp();
        if (correct) {
            smile.gameObject.SetActive(true);
            Debug.Log("回答正確");
        }
        yield return new WaitForSeconds(0.3f);

        smile.gameObject.SetActive(false);

        //保存答案
        StartCoroutine(SaveAns());
        //調整SOA
        if (!isTrial&&(questionIndex==7|| questionIndex == 9 || questionIndex == 11 || questionIndex == 13 || questionIndex == 15 || questionIndex == 17 || questionIndex == 19 )) {
            SOA = step[13];
            Debug.Log("SOA:"+SOA);
        }
        else {
            if (correct)
            {
                cummulatedCorrectNum++;
                if (cummulatedCorrectNum == 2) {
                    currentlevel = currentlevel > min ? currentlevel - 1 : currentlevel;
                    cummulatedCorrectNum = 0;
                }
                
            }
            else
            {
                cummulatedCorrectNum = 0;
                currentlevel = currentlevel < max ? currentlevel + 1 : currentlevel;

            }
            SOA = step[currentlevel];

        }
        

        ShowQuestion();
    }

    IEnumerator SaveAns()
    {
        yield return null;
        if (!isTrial) {
            tmpAnsCollection.Add("{\"userId\":\"" + Controller.instance.userId + "\",\"questionid\":\"" + questionid_db + "\",\"round\":\"" + (3 - TestCount) + "\",\"audioOrder\":\"" + audioOrder + "\",\"correntAns\":\"" + correctAns + "\",\"chooseAns\":\"" + userAns + "\",\"SOA\":\"" + currentSOA + "\",\"correctorNot\":\"" + correctorNot + "\",\"startTimeStamp\":\"" + startTimeStamp + "\",\"endTimeStamp\":\"" + endTimeStamp + "\"}");
            //Controller.instance.myWWW.SaveToDatabase(Controller.instance.userId, questionid_db, "correntAns:" + correctAns + "; chooseAns:" + userAns + ";audioOrder:\"" + audioOrder + "\";SOA:" + currentSOA + ";correctorNot:" + correctorNot, startTimeStamp, endTimeStamp);

        }
    }

    IEnumerator SaveToDataBase() {
        yield return null;
        //把数据上传到数据库
        //questionid_db = (3 - TestCount) * 100 + questionIndex;
        string str = "{ \"Ans\":["+ string.Join(",", (string[])tmpAnsCollection.ToArray(typeof(string)))+"]}";
        if (!isTrial)
        {
            Controller.instance.myWWW.SaveToDatabase(Controller.instance.userId, 100000+ (3 - TestCount) * 1000+1, str, startTimeStamp, endTimeStamp);

        }
    }

}

//task3:增加規則
//
 //在練習階段，從398 （10 2.6）到158 （10 2.2），錯一個則返回到上一級，對一個則升上一級
// 在正式測試階段從398 (10 2.6) 開始， 向上到1（10 0.0）向下 到1000（10 3.0）
 //7-10題加入一個bouns，即都是398 (10 2.6)



