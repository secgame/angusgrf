﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerController : MonoBehaviour
{
    public bool EnableTimer;
    public int TimeLeft;
    public int TotalTime;
    public Text txt_Time;
    public GameObject Timer;

    // Start is called before the first frame update
    void Start()
    {
        EnableTimer = false;
    }


    /// <summary>
    /// 停止計時
    /// </summary>
    public void StopTimer() {
        EnableTimer = false;
    }

    public void BeginTimer() {
        EnableTimer = true;
        StartCoroutine(StartTimer());
    }

    public void PrepareTimer(int time)
    {
        TotalTime = time;
        EnableTimer = true;
        UpdateTimer(true);

    }

    /// <summary>
    /// 開始計時
    /// </summary>
    /// <returns></returns>
    private IEnumerator StartTimer()
    {
        if (!EnableTimer)
            yield break;

        while (TimeLeft > 0 )
        {
            yield return new WaitForSeconds(1);
            UpdateTimer();
        }
    }


    /// <summary>
    /// 更新計時器
    /// </summary>
    /// <param name="init"></param>
    private void UpdateTimer(bool init = false)
    {
        if (EnableTimer)
        {
            if (init)
            {
                Timer.gameObject.SetActive(true);
                TimeLeft = GetTimerAmount();
            }
            else
                TimeLeft--;

            txt_Time.text = TimeLeft/60+"分"+TimeLeft%60+"秒";
        }
        else if (!EnableTimer && init)
            Timer.gameObject.SetActive(false);
    }

    private int GetTimerAmount() {
        return TotalTime;
    }
}
