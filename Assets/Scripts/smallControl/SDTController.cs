﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SDTController : MonoBehaviour
{
    int Statue = 0; 
    //0->總介紹, 1->練習，2-> 正式測試的介紹 3.正式測試
    public Image ins_img;
    public Button btn_continue;
    public Button btn_back;
    public GameObject QuestionControl;
    public CanvasGroup QuestionPanelHolder;
    public Button btn_Yes;
    public Button btn_No;
    public Image cry;
    public Image happy;
    System.Random rd = new System.Random();

    public int questonAmount;
    public int questionIndex;

    public Sprite[] ins_Sprites;
    public AudioClip[] ins_audios;

    int TestCount;
    bool isTrial;

    public AudioClip[] soundAudio; //0是有生聲音，1是沒有聲音
    public int[] soundArr; //2是固定的B聲, 1是空的或有聲音的

    public int accYesNum;
    public int accNoNum;

    public int SOA;
    public int Ans;

    string startTimeStamp;
    string endTimeStamp;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame() {
        Statue = 0;
        soundArr = new int[2];
        ShowStatue();
        accYesNum = 0;
        accNoNum = 0;
    }

    public void ShowStatue() {
        switch (Statue) {
            case 0: 
                ins_img.gameObject.SetActive(true);
                btn_continue.gameObject.SetActive(true);
                btn_back.gameObject.SetActive(false);
                QuestionControl.gameObject.SetActive(false);
                ins_img.sprite = ins_Sprites[0];
                StartCoroutine(PlayInstructionSound(0));
                accYesNum = 0;
                accNoNum = 0;
                Controller.instance.QuestionNumText.gameObject.SetActive(false);
                break;
            case 1:
                StopAllCoroutines();
                ins_img.gameObject.SetActive(false);
                btn_continue.gameObject.SetActive(false);
                btn_back.gameObject.SetActive(false);
                QuestionControl.gameObject.SetActive(false);
                Controller.instance.QuestionNumText.gameObject.SetActive(true);
                ShowQuestion();
                break;
            case 2:
                StopAllCoroutines();
                ins_img.gameObject.SetActive(true);
                btn_continue.gameObject.SetActive(true);
                btn_back.gameObject.SetActive(true);
                QuestionControl.gameObject.SetActive(false);
                ins_img.sprite = ins_Sprites[1];
                StartCoroutine(PlayInstructionSound(1));
                Controller.instance.QuestionNumText.gameObject.SetActive(false);
                accYesNum = 0;
                accNoNum = 0;
                break;
            case 3:
                StopAllCoroutines();
                ins_img.gameObject.SetActive(false);
                btn_continue.gameObject.SetActive(false);
                btn_back.gameObject.SetActive(false);
                QuestionControl.gameObject.SetActive(false);
                Controller.instance.QuestionNumText.gameObject.SetActive(true);
                ShowQuestion();
                break;
        }
    }

    public void ContinueClick(bool willContinue) {
        if (willContinue)
        {
            switch (Statue)
            {
                case 0:
                    Statue = 1;
                    isTrial = true;
                    questonAmount = 10;
                    questionIndex = 1;
                    ShowStatue();
                    break;
                case 2:
                    Statue = 3;
                    isTrial = false;
                    questonAmount = 10;
                    questionIndex = 1;                   
                    ShowStatue();
                    break;

            }
        }
        else {
            Statue = 1;
            isTrial = true;
            questonAmount = 10;
            questionIndex = 1;
            ShowStatue();
        }
        
    }


    int currentanimalIndex = 0;
    public void DisplaySounds() {
        if (currentanimalIndex < soundArr.Length)
        {
            StartCoroutine(PlaySound(soundArr[currentanimalIndex]));
        }
        else {
            Invoke("showChoice", 1.5f);
        }
        /*else {
            if (Statue == 1)
            {
                Statue = 2;
                QuestionPanelHolder.blocksRaycasts = true;
                ShowStatue();
            }
            else {            
                ball.gameObject.SetActive(false);
                ControlPanelHolder.blocksRaycasts = true;
                startTimeStamp = Controller.GetTimeStamp();
            }
           
        }*/
    }

    public void showChoice() {
        QuestionControl.gameObject.SetActive(true);
        QuestionPanelHolder.blocksRaycasts = true;
        startTimeStamp = Controller.GetTimeStamp();
    }

    IEnumerator PlayInstructionSound(int index)
    {
        Controller.instance.PlaySound(ins_audios[index]);
        btn_continue.interactable = false;
        btn_back.interactable = false;
        yield return new WaitForSeconds(ins_audios[index].length);
        btn_continue.interactable = true;
        btn_back.interactable = true;
    }

    IEnumerator PlaySound(int index) {
        if (currentanimalIndex == 0)
        {
            yield return new WaitForSeconds( 0.02f);
        }
        else {
            yield return new WaitForSeconds(SOA * 0.001f);
        }
        
        QuestionPanelHolder.blocksRaycasts = false;
        Controller.instance.PlaySound(soundAudio[index]);
        StartCoroutine(AudioPlayFinished(75*0.001f));
        currentanimalIndex++;
    }

    private IEnumerator AudioPlayFinished(float time)
    {   
        
        yield return new WaitForSeconds(time);
        Controller.instance.StopAudioPlayer();
        DisplaySounds();
    }

    public void ShowQuestion() {

        if ((questonAmount - questionIndex) < 0) {
            if (isTrial)
            {
                Statue = 2;
                ShowStatue();
                Invoke("ShowStatue", 0.3f);
            }
            else {
                Gameover();
            }
        }

        happy.gameObject.SetActive(false);
        cry.gameObject.SetActive(false);

        currentanimalIndex = 0;
        Ans = rd.Next(0, 2);
        if (Ans == 1)
            accYesNum++;
        if (Ans == 0)
            accNoNum++;

        if (accYesNum > 5)
        {
            Ans = 0;
            accNoNum++;
        }

        if (accNoNum > 5)
        {
            Ans = 1;
            accYesNum++;
        }

        soundArr[0] = 2;
        soundArr[1] = Ans;
       

        //task1: 增加題目進度顯示
        if (isTrial)
        {
            if (questionIndex <= questonAmount)
                 Controller.instance.QuestionNumText.text = "答題進度(練習): " + questionIndex + "/" + questonAmount;
        }
        else
        {
            if (questionIndex <= questonAmount)
              Controller.instance.QuestionNumText.text = "答題進度: " + questionIndex + "/" + questonAmount;
        }
        Debug.Log("Question" + questionIndex + "：[B,sound]:[" + soundArr[0] + "," + soundArr[1] + "]");
        if (isTrial) {
            SOA = questionIndex % 2 == 0 ? 2500 : 1500;
        }
        else {
            SOA = 1500;
        }

        if (questionIndex <= questonAmount) 
            DisplaySounds(); 


        /*
        CancelInvoke();
        btn_Mouse.gameObject.SetActive(true);
        btn_Cow.gameObject.SetActive(true);

        if ((questonAmount-questionIndex) <0) {
            if (Statue == 1)
            {
                Statue = 2;
                ShowStatue();
                Invoke("ShowStatue", 0.3f);
            }
            else {
                if (!isTrial)
                 {
                    TestCount--;
                    if (TestCount <= 0)
                    {
                        Gameover();
                        //更新題目列表
                    }
                    else {

                    }
                }

                Statue = 4;
                Invoke("ShowStatue", 0.3f);
            }
            return;
        }
        currentanimalIndex = 0;
        animalSounds.Clear();
        int first = rd.Next(0,2);
        int second = rd.Next(0,2);
        //task1: 增加題目進度顯示
        if (isTrial)
        {
            Controller.instance.QuestionNumText.text = "答题进度(練習): " +questionIndex+ "/" + questonAmount;
        }
        else {
            Controller.instance.QuestionNumText.text = "答题进度: " + questionIndex + "/" + questonAmount;
        }
        Debug.Log("Question"+ questionIndex + "：[first,second]:[" + first + "," + second + "]");
        Ans = first;
        animalSounds.Add(first);
        animalSounds.Add(second);
        SOA = 386;
        ball.gameObject.SetActive(true);
        ControlPanelHolder.blocksRaycasts = false;
        DisplaySounds();*/
    }

    private void Gameover()
    {
        string str = Controller.instance.hasFinished;
        if (str == "")
        {
            str = "1";
        }
        else
        {
            str = str + ",1";
        }

        PlayerPrefs.SetString("hasFinished", str);
        Controller.instance.hasFinished = str;
        //保存到數據庫
        StartCoroutine(Controller.instance.myWWW.UpdateUser());
        Controller.instance.GameOver();
    }

    int questionid_db;
    string correctAns;
    string userAns;
    float currentSOA;
    bool correctorNot;

    public void JudgeQuestion(int ans) {
        questionid_db = 1000 + questionIndex;
        correctAns = Ans == 1 ? "Yes" : "No";
        userAns = ans == 1 ? "Yes" : "No";
        currentSOA = SOA;
        correctorNot = (ans == Ans);
        StartCoroutine(ShowSmile(correctorNot));
        questionIndex++;

    }

    IEnumerator ShowSmile(bool correct) {
        endTimeStamp = Controller.GetTimeStamp();
        QuestionPanelHolder.blocksRaycasts = false;
        if (isTrial) {
            if (correct)
            {
                happy.gameObject.SetActive(true);
                cry.gameObject.SetActive(false);
                Debug.Log("回答正確");
            }
            else
            {
                cry.gameObject.SetActive(true);
                happy.gameObject.SetActive(false);
                Debug.Log("回答錯誤");
            }
        }
        
        yield return new WaitForSeconds(0.3f);
        QuestionControl.SetActive(false);
        

        if (!isTrial) {
            StartCoroutine(SaveAns());
        }
        
        ShowQuestion();
    }

    IEnumerator SaveAns()
    {
        yield return null;
        //把数据上传到数据库
        //questionid_db = (3 - TestCount) * 100 + questionIndex;
        Debug.Log(Controller.instance.userId + "," + questionid_db + "," + "correntAns:" + correctAns + "; chooseAns:" + userAns + ";SOA:" + currentSOA + ";correctorNot:" + correctorNot + "," + startTimeStamp + "," + endTimeStamp);
        if (!isTrial) {
            Controller.instance.myWWW.SaveToDatabase(Controller.instance.userId, questionid_db, "correntAns:" + correctAns + "; chooseAns:" + userAns + ";SOA:" + currentSOA + ";correctorNot:" + correctorNot, startTimeStamp, endTimeStamp);

        }
    }





    //task3:增加規則

}
