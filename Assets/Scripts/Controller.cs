﻿#if (UNITY_ANDROID || UNITY_IOS)
#define SUPPORTED_PLATFORM
#endif

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using QuizApp.Attributes;
using AudioRecorder;//用來錄音用的
using UnityEngine.SceneManagement;
using System;



#if USE_DOTWEEN
using DG.Tweening;
using UnityEngine.Events;
using System;
#endif

#if UNITY_EDITOR
using UnityEditor;
#endif

public class Controller : MonoBehaviour
{
    internal static Controller instance;

    [QAHeader("Canvases")]

    public GameObject HomeCanvas;
    public GameObject CategoriesCanvas;
    public GameObject GameCanvas;
    public GameObject PauseCanvas;
    public GameObject GameOverCanvas;

    [QAHeader("Home Canvas Objects")]

    public Text TopbarTitle;
    public GameObject HighscorePage;
    public GameObject SettingsPage;
    public GameObject BackButton;
    public GameObject ProfileButton;
    public GameObject SettingsButton;
    public GameObject StartButton;

    [QAHeader("Instruction Objects")]
    public GameObject InstructionCanvas;
    public int hasReadInstruction;

    [QAHeader("Settings Page Objects")]

    public Button CacheButton;
    public Text CacheText;


    [QAHeader("Category Canvas Objects")]

    public GameObject CategoryGroup;
    public GameObject ErrorPanel;
    public GameObject InfoPanel;
    public GameObject LoadingAnimation;

    [HideInInspector]
    public Animation CategoryLoading;
    [HideInInspector]
    public CategoriesParent CategoriesParentScript;

    [QAHeader("Game Canvas Objects")]

    public Text CategoryName;
    public Text TimerText;
    public Text ScoreText;
    public Text LivesText;
    public Text QuestionNumText;
    public Text MainQuestionDisplay;
    public Text AltQuestionDisplay;
    public RawImage ImageDisplay;
    public Image ImageParent;
    public GameObject AnswerBarsParent;
    public GameObject ShortAnswerBarsParent;
    public GameObject ToFAnswersParent;
    public GameObject TOTPanel;
    public GameObject SDTPanel;
    public Text ToFAnswersHeading;
    public GameObject PostQuestionButtons;
    public Text ExplanationText;
    public RawImage ExplanationImage;
    public GameObject ExplanationAudio;
    public GameObject ExplanationButton;
    public GameObject ExplanationPanel;
    public GameObject ExtraLifeButton;
    public GameObject JudgePanel;
    public GameObject MarkingPanel;
    public JudgeController MyJudgeController;
    public MarkChoice myMarkChoice;
    public GameObject ChoosePicturePanel;
    public GameObject FrogMap;
    public GameObject AutoVsPanel;
    public Text ExplanationDisplay;
    public Animation ImageDownloadAnimation;
    public GameObject QuitGamePanel;
    public GameObject RecordingPage;
    public GameObject StartBtn;
    public GameObject PlayBtn;
    public GameObject SaveBtn;
    public GameObject DrawingPage;
    public GameObject SaveDrawingBtn;
    public GameObject ClearDrawingBtn;
    public bool autoPlay;
    public GameObject darwImage;
    public int userId;
    public string hasFinished;
    public bool ordered;

    [QAHeader("Top Canvas Objects")]
    public GameObject WholeTimer;


    public WWWController myWWW;

    [HideInInspector]
    public AspectRatioFitter ImageAspectRatioFitter;
    [HideInInspector]
    public CanvasGroup AnswerBarsCanvasGroup;
    [HideInInspector]
    public CanvasGroup ShortAnswerBarsCanvasGroup;
   [HideInInspector]
    public CanvasGroup PictureAnswerBarsCanvasGroup;
    [HideInInspector]
    public GameObject[] ToFAnswerHolder;
    [HideInInspector]
    public CanvasGroup ToFAnswerHoldersCanvasGroup;
    [HideInInspector]
    public CanvasGroup RecordingHolderGroup;
   
    public CanvasGroup DrawingHolderGroup;
    [HideInInspector]
    public Text[] AnswerDisplay;
    [HideInInspector]
    public Image[] AnswerParent;
    [HideInInspector]
    public Text[] ShortAnswerDisplay;
    [HideInInspector]
    public Image[] ShortAnswerParent;
    [HideInInspector]
    public Image[] PictureAnswerParent;
    [HideInInspector]
    public Animation LivesAnimation;
    [HideInInspector]
    public Animation ScoreAnimation;

    [QAHeader("GameOver Canvas Objects")]

    public Image CategoryImage;
    public Text CategoryNameText;
    public Text GameScoreText;
    public Text HighscoreText;

    [QAHeader("Pasused Canvas Objects")]

    public Text Contenttxt;


    [QAHeader("Game Variables")]

    [Space(5)]

    public bool SkipCategoriesDisplay = false;

    [QASeparator]

    public bool EnablePausing = true;

    [QASeparator]

    [Range(5, 60)]
    public int DownloadTimeout = 15;

    [QASeparator]

    public bool EnableTimer = false;
    [Range(5, 100)]
    public int UniversalTimerAmount = 10000;
    [Range(1, 100)]
    public int PointsPerSecond = 10;

    [Space(5)]

    [Range(1, 1000)]
    [Help("Points Per Question is used to calculate the score ONLY if you disable the timer. Otherwise, Points Per Second is used.")]
    public int PointsPerQuestion = 100;

    [QASeparator]

    public bool EnableLives = false;
    [Range(1, 100)]
    public int UniversalLivesAmount = 1000;
    [QASeparator]
    public bool EnableQuestionNum = true;
    [Range(1, 100)]
    public int UniversalQuestionNum = 200;

    [QASeparator]
    public bool ShowPostQuestionButtons;
    [Range(1, 30)]
    public int PostQuestionDelay = 3;

    [QASeparator("Colors")]

    public Color DefaultColor = Color.white;
    public Color CorrectColor = Color.green;
    public Color WrongColor = Color.red;
    public Color ChoosenColor = Color.yellow;

    [QASeparator("Sounds")]

    public AudioClip CorrectSound;
    public AudioClip WrongSound;
    public AudioClip TickingSound;
    public AudioClip InstructionSound;

    [HideInInspector]
    public List<QuestionFormat> QuestionList = new List<QuestionFormat>();
    public List<frogSetting> FrogList = new List<frogSetting>();
    public List<VSSetting> VSList = new List<VSSetting>();
    [HideInInspector]
    // public List<AnswerFormat> AnswersList = new List<AnswerFormat>();
    public List<ChoiceDetailsFormat> AnswersList = new List<ChoiceDetailsFormat>();

    //[HideInInspector]
    public AudioSource AudioPlayer = new AudioSource();
    public Recorder recorder = new Recorder();

    private const string SoundPref = "SoundState";

    private int SoundState, QuestionsLimit, TimeLeft, LivesLeft, QuestionLeft,TotalQuestionNum, CurrentScore;
    public int CurrentQuestion;
    private bool isMainMenu, isSettings, isProfile, isCategories, ResetAnswerColors, isHybrid, Answered, isGameOver, isInGame, Paused, Ticking, RecordingQuestion,DrawingQuestion, WaitingForFile, WaitingForImage;

    private CategoryFormat CurrentCategory = new CategoryFormat();

    private List<string> ImageLinks = new List<string>();
    private List<string> FaultyLinks = new List<string>();

    public string startTimeStamp = "";
    public string endTimeStamp = "";

    private AspectRatios AspectRatio = new AspectRatios();

    //总的时间
    public int totalSeconds;
    public bool recorderEmpty;

    //给PPTV使用的
    ArrayList cummulatedQueNum = new ArrayList();
    int cummulatedQueIndex = 0;
    ArrayList cummulatedCorrect = new ArrayList();

    //给能够半途退出使用
    List<User_Record> UserRecord = new List<User_Record>();

    //重新加载的次数
    int ReloadRimes = 0;




#if UNITY_EDITOR
    private bool DebugAutoAnswer = true;
#endif

#if USE_ADMOB
    private bool RewardedShown;
    private int TotalGameOvers;
#endif

#if UNITY_EDITOR
    [ContextMenu("Fetch Constants")]
    public void FetchConstants()
    {
        LoadingAnimation.SetActive(false);
        CategoryLoading = LoadingAnimation.GetComponent<Animation>();

        AnswerBarsCanvasGroup = AnswerBarsParent.GetComponent<CanvasGroup>();
        ShortAnswerBarsCanvasGroup = ShortAnswerBarsParent.GetComponent<CanvasGroup>();
        ToFAnswerHoldersCanvasGroup = ToFAnswersParent.GetComponent<CanvasGroup>();

        ToFAnswerHolder = new GameObject[2];
        ToFAnswerHolder[0] = ToFAnswersParent.transform.GetChild(1).gameObject;
        ToFAnswerHolder[1] = ToFAnswersParent.transform.GetChild(2).gameObject;

        int childCount = AnswerBarsParent.transform.childCount;
        

        AnswerDisplay = new Text[childCount];
        AnswerParent = new Image[childCount];

        for (int x = 0; x < childCount; x++)
        {
            AnswerParent[x] = AnswerBarsParent.transform.GetChild(x).gameObject.GetComponent<Image>();
            AnswerDisplay[x] = AnswerParent[x].transform.GetChild(0).gameObject.GetComponent<Text>();
        }

        childCount = ShortAnswerBarsParent.transform.childCount;
        Debug.Log("childCount+:"+ childCount);
        ShortAnswerDisplay = new Text[childCount];
        ShortAnswerParent = new Image[childCount];

        for (int x = 0; x < childCount; x++)
        {
            ShortAnswerParent[x] = ShortAnswerBarsParent.transform.GetChild(x).gameObject.GetComponent<Image>();
            ShortAnswerDisplay[x] = ShortAnswerParent[x].transform.GetChild(0).gameObject.GetComponent<Text>();
        }

        AudioPlayer = GetComponent<AudioSource>();

        CategoriesParentScript = CategoryGroup.GetComponentInChildren<CategoriesParent>(true);

        LivesAnimation = LivesText.GetComponent<Animation>();
        ScoreAnimation = ScoreText.GetComponent<Animation>();

        ImageAspectRatioFitter = ImageDisplay.GetComponent<AspectRatioFitter>();
    }

    [ContextMenu("Open Cache Location", false, 9999999)]
    public void OpenCacheLocation()
    {
        Application.OpenURL(Application.temporaryCachePath);
    }

    [ContextMenu("Generate Debug Data", false, 9999999)]
    public void GenerateDebugData()
    {
        string path = EditorUtility.SaveFilePanel("Save Location", "", string.Concat("Controller-Debug ", System.DateTime.Now.ToString("yyyy-MM-dd_hh-mm-ss tt")), "json");

        if (string.IsNullOrEmpty(path))
            return;

        File.WriteAllText(path, JsonUtility.ToJson(this, true));
    }
#endif

    // Use this for initialization
    private void Awake()
    {
        if (instance == null)
            instance = this;

        //ToggleSound(true);
        //ToggleVibration(true);

        myWWW = this.GetComponent<WWWController>();

        userId = PlayerPrefs.GetInt("scolarId");
        hasFinished = PlayerPrefs.GetString("hasFinished");
        totalSeconds = PlayerPrefs.GetInt("totalSeconds");
        hasReadInstruction = PlayerPrefs.GetInt("hasReadInstruction",0);//0->還沒有閱讀； 1->已閱讀
        
        GetAspectRatio();      
    }

    public static string GetTimeStamp()
    {
        System.TimeSpan ts = System.DateTime.UtcNow - new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
        return System.Convert.ToInt64(ts.TotalMilliseconds).ToString();
    }


    private void GetAspectRatio()
    {
        float aspect = Camera.main.aspect;

        if (aspect >= 0.749)
            AspectRatio = AspectRatios._3x4;
        else if (aspect >= 0.666)
            AspectRatio = AspectRatios._2x3;
        else if (aspect >= 0.624)
            AspectRatio = AspectRatios._10x16;
        else
            AspectRatio = AspectRatios.Default;
    }

    private void Start()
    {
#if USE_ADMOB
        AdmobManager.instance.CheckConsent();
        TotalGameOvers = 0;
#endif

        //所有的參數都要初始化
        SoundState = CurrentQuestion = QuestionsLimit = TimeLeft = LivesLeft = QuestionLeft = CurrentScore = 0;
        isMainMenu=isSettings=isProfile=isCategories=ResetAnswerColors=isHybrid=Answered=isGameOver=isInGame=Paused=Ticking=RecordingQuestion=DrawingQuestion=WaitingForFile=WaitingForImage= false;

        if ( totalSeconds== 0)
        {
            WholeTimer.SetActive(false);
        }
        else {
            WholeTimer.SetActive(true);
            WholeTimer.GetComponent<TimerController>().PrepareTimer(totalSeconds);
            WholeTimer.GetComponent<TimerController>().BeginTimer();
        }

        
        ShowHome();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isMainMenu)
            {
#if UNITY_EDITOR
                EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
            }
            else if (isSettings || isCategories || isProfile)
                ShowHome();
            else if (isInGame)
                PauseGame();
        }
    }



    /// <summary>
    /// 播放音效
    /// </summary>
    /// <param name="Clip"></param>
    public void PlaySound(AudioClip Clip)
    {
        InstructionSound = Clip;
        StopAudioPlayer();

        AudioPlayer.PlayOneShot(Clip);
        Controller.instance.ExplanationAudio.SetActive(true);
        StartCoroutine(AudioAnimationFinished(Clip));
    }

    public void PlaySound()
    {

        StopAudioPlayer();

        AudioPlayer.PlayOneShot(InstructionSound);
    }

    public void PlayButtonClick() {
        StopAudioPlayer();
        AudioPlayer.PlayOneShot(TickingSound);
    }

    private IEnumerator AudioAnimationFinished(AudioClip Clip) {
        ExplanationAudio.GetComponent<Animator>().SetBool("speaking", true);
        float time = Clip.length;
        yield return new WaitForSeconds(time);
        ExplanationAudio.GetComponent<Animator>().SetBool("speaking", false);
    }


    private IEnumerator AudioPlayFinished(float time)
    {
        //繼續按鈕隱藏
        ExplanationButton.GetComponent<Button>().interactable = false;
        //隱藏開始錄音按鈕
        StartBtn.GetComponent<Button>().interactable = false;

        //隱藏選項按鈕
        foreach (Transform tmp in AnswerBarsParent.transform)
        {
            tmp.gameObject.GetComponent<Button>().interactable = false;
        }
        foreach (Transform tmp in ShortAnswerBarsParent.transform)
        {
            tmp.gameObject.GetComponent<Button>().interactable = false;
        }
        foreach (Transform tmp in ChoosePicturePanel.transform)
        {
            tmp.gameObject.GetComponent<Button>().interactable = false;
        }

        //隱藏繪畫按鈕
        ClearDrawingBtn.GetComponent<Button>().interactable = false;
        SaveDrawingBtn.GetComponent<Button>().interactable = false;


        //播放喇叭動畫
        ExplanationAudio.GetComponent<Animator>().SetBool("speaking", true);
        yield return new WaitForSeconds(time);
        //隱藏喇叭動畫
        ExplanationAudio.GetComponent<Animator>().SetBool("speaking", false);
        //顯示繼續按鈕
        ExplanationButton.GetComponent<Button>().interactable = true;
        //顯示開始錄音按鈕
        StartBtn.GetComponent<Button>().interactable = true;
        //顯示問題按鈕
        foreach (Transform tmp in AnswerBarsParent.transform)
        {
            tmp.gameObject.GetComponent<Button>().interactable = true;
        }
        foreach (Transform tmp in ShortAnswerBarsParent.transform)
        {
            tmp.gameObject.GetComponent<Button>().interactable = true;
        }
        foreach (Transform tmp in ChoosePicturePanel.transform)
        {
            tmp.gameObject.GetComponent<Button>().interactable = true;
        }

        //顯示繪畫按鈕
        ClearDrawingBtn.GetComponent<Button>().interactable = true;
        SaveDrawingBtn.GetComponent<Button>().interactable = true;

       //是錄音題，而且只能錄製一次，而且還要看題卡的話，就不能馬上開始計時
       if (!(QuestionList[CurrentQuestion].IsRecording && QuestionList[CurrentQuestion].QuestionImage != "" && QuestionList[CurrentQuestion].RecordingLimited != "") && QuestionList[CurrentQuestion].QuestionAudio != "")
       {
           StartCoroutine(StartTimer());
           Debug.Log("startTimer: afterAudioPlay");
       }

        //如果录音只能听一次的话，那么就需要
        if (QuestionList[CurrentQuestion].RecordingLimited == "1") {
            ExplanationAudio.SetActive(false);
        }

    }

    private void PlaySound(string name) {

        InstructionSound = null;
        StopAudioPlayer();
        InstructionSound = (AudioClip)Resources.Load(name);
        //Debug.Log((InstructionSound == null) ? "Sound is null" : "Sound is not null");
        if (InstructionSound == null) {
            //PlaySound(name);
            return;
        }



        AudioPlayer.PlayOneShot(InstructionSound);
        StartCoroutine(AudioPlayFinished(InstructionSound.length));
        /*//如果是問答的話，長度減去10秒
        if (QuestionList[CurrentQuestion].TextIndex == "3")
        {
            StartCoroutine(AudioPlayFinished(InstructionSound.length-10));
        }
        else {
            StartCoroutine(AudioPlayFinished(InstructionSound.length));
        }*/


    }



    /// <summary>
    /// 停止播放音效
    /// </summary>
    public void StopAudioPlayer()
    {
        if (AudioPlayer.isPlaying)
            AudioPlayer.Stop();
        ExplanationButton.GetComponent<Button>().interactable = true;
        StartBtn.GetComponent<Button>().interactable = true;

        //隱藏選項按鈕
        foreach (Transform tmp in AnswerBarsParent.transform)
        {
            tmp.gameObject.GetComponent<Button>().interactable = true;
        }
        foreach (Transform tmp in ShortAnswerBarsParent.transform)
        {
            tmp.gameObject.GetComponent<Button>().interactable = true;
        }
        foreach (Transform tmp in ChoosePicturePanel.transform)
        {
            tmp.gameObject.GetComponent<Button>().interactable = true;
        }

    }

    private void PauseAudioPlayer() {
        if (AudioPlayer.isPlaying)
            AudioPlayer.Pause();
    }

    private void RestartAudioPlayer()
    {
        AudioPlayer.UnPause();
    }

    /// <summary>
    /// 顯示主頁
    /// </summary>
    public void ShowHome()
    {
        StopAudioPlayer();

        HomeCanvas.SetActive(true);
        CategoriesCanvas.SetActive(false);
        GameCanvas.SetActive(false);
        PauseCanvas.SetActive(false);
        GameOverCanvas.SetActive(false);

        SettingsPage.SetActive(false);

        BackButton.SetActive(true);
        SettingsButton.SetActive(false);

        TopbarTitle.text = "主 頁";

        isGameOver = isMainMenu = true;
        isSettings = isProfile = isCategories = isInGame = Paused = false;

        //判斷是不是顯示開始玩的按鈕
        if (hasReadInstruction == 0)
        {
            StartBtn.SetActive(false);
        }
        else {
            StartBtn.SetActive(true);
        }
       
    }

    public void ShowInstruction(bool show) {
        HomeCanvas.SetActive(!show);
        InstructionCanvas.SetActive(show);
    }

    public void ShowVideo()
    {
        SceneManager.LoadScene("Video");
    }

    public void ChangeHasReadInstruction()
    {
        hasReadInstruction = 1;
        PlayerPrefs.SetInt("hasReadInstruction", 1);//0->還沒有閱讀； 1->已閱讀
    }

    /// <summary>
    /// 顯示設置
    /// </summary>
    public void ShowSettings()
    {
        SettingsPage.SetActive(true);

        BackButton.SetActive(true);
        SettingsButton.SetActive(false);


        TopbarTitle.text = "Settings";

        isSettings = true;
        isMainMenu = false;
    }

    /// <summary>
    /// 顯示最高分
    /// </summary>
    public void ShowHighscores()
    {
        HighscorePage.SetActive(true);

        BackButton.SetActive(true);
        //ProfileButton.SetActive(false);
        SettingsButton.SetActive(false);

        TopbarTitle.text = "Highscores";

        isProfile = true;
        isMainMenu = false;
    }

    /// <summary>
    /// 轉換聲音狀態
    /// </summary>
    /// <param name="Init"></param>
   /* public void ToggleSound(bool Init = false)
    {
        SoundState = PlayerPrefs.GetInt(SoundPref, 1);

        if (Init)
        {
            if (SoundState == 0)
            {
                SoundEnabled = false;
                SoundButton.image.color = Color.red;
            }
            else if (SoundState == 1)
            {
                SoundEnabled = true;
                SoundButton.image.color = Color.green;
            }
            else
            {
                PlayerPrefs.SetInt(SoundPref, 1);
                SoundEnabled = true;
                SoundButton.image.color = Color.green;
            }
        }
        else
        {
            if (SoundState == 0)
            {
                SoundEnabled = true;
                PlayerPrefs.SetInt(SoundPref, 1);
                SoundButton.image.color = Color.green;
            }
            else if (SoundState == 1)
            {
                SoundEnabled = false;
                PlayerPrefs.SetInt(SoundPref, 0);
                SoundButton.image.color = Color.red;
            }
        }
    }*/

    /// <summary>
    /// 調整震動
    /// </summary>
    /// <param name="Init"></param>
  /*  public void ToggleVibration(bool Init = false)
    {
#if (SUPPORTED_PLATFORM || UNITY_EDITOR)
        VibrationState = PlayerPrefs.GetInt(VibrationPref, 1);

        if (Init)

        {
            if (VibrationState == 0)
            {
                VibrationEnabled = false;
                VibrationButton.image.color = Color.red;
            }
            else if (VibrationState == 1)
            {
                VibrationEnabled = true;
                VibrationButton.image.color = Color.green;
            }
            else
            {
                VibrationEnabled = true;
                PlayerPrefs.SetInt(VibrationPref, 1);
                VibrationButton.image.color = Color.green;
            }
        }
        else
        {

            if (VibrationState == 0)
            {
                VibrationEnabled = true;
                PlayerPrefs.SetInt(VibrationPref, 1);
                VibrationButton.image.color = Color.green;
            }
            else if (VibrationState == 1)
            {
                VibrationEnabled = false;
                PlayerPrefs.SetInt(VibrationPref, 0);
                VibrationButton.image.color = Color.red;
            }
        }
#else
        VibrationButton.gameObject.SetActive(false);
#endif
    }*/

    /// <summary>
    /// 清除緩存
    /// </summary>
    public void DeleteCache()
    {
        PlayerPrefs.SetString("scolarId", "");
        PlayerPrefs.SetString("hasFinished", "");
        PlayerPrefs.SetInt("hasReadInstruction", 0);

        DirectoryInfo dir = new DirectoryInfo(Application.temporaryCachePath);

        foreach (FileInfo file in dir.GetFiles())
            file.Delete();

        StartCoroutine(DeletionComplete());
    }

    private IEnumerator DeletionComplete()
    {
        string OriginalText = CacheText.text;

        CacheButton.interactable = false;
        CacheText.text = "Done";

        yield return new WaitForSeconds(3f);

        CacheButton.interactable = true;
        CacheText.text = OriginalText;

        SceneManager.LoadScene("Login");
    }

    /// <summary>
    /// 顯示類別
    /// </summary>
    public void ShowCategories()
    {
        HomeCanvas.SetActive(false);
        CategoriesCanvas.SetActive(true);

        InfoPanel.SetActive(false);
        ErrorPanel.SetActive(false);
        StopAudioPlayer();

        if (LoadingAnimation.activeInHierarchy)
        {
            CategoryLoading.Stop();
            LoadingAnimation.SetActive(false);
        }

        isCategories = true;
        isMainMenu = false;

        if (SkipCategoriesDisplay)
        {
            LoadCategory(CategoriesParentScript.GetSingleCategory().Category);
            CategoryGroup.SetActive(false);
        }
        else
            CategoryGroup.SetActive(true);
    }

    /// <summary>
    /// 從遊戲界面退回到類別頁面
    /// </summary>
    public void BeforeShowCategories() {
        AudioPlayer.Stop();
        ShowCategories();
    }

    public void ShowLogin() {
        SceneManager.LoadScene("Login");
    }

    /// <summary>
    /// 加載類別
    /// </summary>
    /// <param name="format"></param>
    internal void LoadCategory(CategoryFormat format)
    {
        CurrentCategory = format;

        isHybrid = false;

        

        if (CurrentCategory.Mode == DownloadMode.Online)
            StartCoroutine(DownloadJSON());
        else if (CurrentCategory.Mode == DownloadMode.Offline)
            //StartCoroutine(DownloadJSON());
            ParseJSON(CurrentCategory.OfflineFile.text);
        else
            StartCoroutine(DownloadJSON(true));
    }

    public void SetUserRecord(List<User_Record> userRecord) {
        UserRecord = userRecord;

        Debug.Log("UserRecord.count:"+UserRecord.Count);
        Debug.Log("TotalQuestionNum/2:" + Mathf.FloorToInt(TotalQuestionNum / 2));

        //判断数目是否超过一半，如果没有，则清空UserRecord
        if (UserRecord.Count < Mathf.FloorToInt(TotalQuestionNum / 2)) {        
            UserRecord.Clear();
        }
    }



    /// <summary>
    /// 解析JSON數據
    /// </summary>
    /// <param name="Content"></param>
    private void ParseJSON(string Content)
    {
        if (isMainMenu)
            return;

        QuestionList.Clear();
        VSList.Clear();
        FrogList.Clear();
        UserRecord = new List<User_Record>();

        if (CurrentCategory.SpecialTemplate == CategoryFormat.TestTemplate.Common || CurrentCategory.SpecialTemplate == CategoryFormat.TestTemplate.PPTV)
        {           
            QuestionsContainer container = JsonUtility.FromJson<QuestionsContainer>(Content);
            QuestionList = container.Questions;
            container = new QuestionsContainer();
            
        }

        if (CurrentCategory.SpecialTemplate == CategoryFormat.TestTemplate.FrogMap) {
            Debug.Log("frogMap content:" + Content);
            FrogSettingContainer container = JsonUtility.FromJson<FrogSettingContainer>(Content);
            FrogList = container.frogSettings;
            ordered = container.Ordered;
            container = new FrogSettingContainer();
            Debug.Log("FrogList:" + FrogList.Count);
        }

        if (CurrentCategory.SpecialTemplate == CategoryFormat.TestTemplate.AutoVisualSearch) {
            Debug.Log("AutoVisualSearch content:" + Content);
            VSSettingContainer container = JsonUtility.FromJson<VSSettingContainer>(Content);
            VSList = container.vsSettings;
            container = new VSSettingContainer();
            Debug.Log("VSList:" + VSList.Count);
        }

        PrepareGame();
    }

    public void ReloadJSON() {
        StartCoroutine(DownloadJSON());
    }


    private IEnumerator DownloadJSON(bool Hybrid = false)
    {
        isHybrid = Hybrid;

        LoadingAnimation.SetActive(true);

        CategoryLoading.Play();

        CategoryGroup.SetActive(false);

        ErrorPanel.SetActive(false);

        using (UnityWebRequest uwr = UnityWebRequest.Get(CurrentCategory.OnlinePath))
        {
            uwr.timeout = DownloadTimeout;

            yield return uwr.SendWebRequest();

            CategoryLoading.Stop();
            LoadingAnimation.SetActive(false);

            if (uwr.isHttpError || uwr.isNetworkError)
            {
                if (Hybrid)
                {
                    ParseJSON(CurrentCategory.OfflineFile.text);
                    CurrentCategory.Mode = DownloadMode.Offline;
                }
                else
                {
                    if (ReloadRimes < 6)
                    {
                        StartCoroutine(DownloadJSON());
                        ReloadRimes++;
                    }
                    else {
                        ErrorPanel.SetActive(true);
                        ReloadRimes = 0;
                    }
                                      

                }
                    
            }
            else
            {
                if (Hybrid)
                    CurrentCategory.Mode = DownloadMode.Online;

                ParseJSON(uwr.downloadHandler.text);
            }

        }
    }

    private void PrepareGame()
    {
#if USE_ADMOB
        AdmobManager.instance.RequestInterstitial();

        if (EnableLives && ShowPostQuestionButtons)
        {
            AdmobManager.instance.RequestRewardBasedVideo();
            RewardedShown = false;
        }
#endif

       

        isCategories = false;

        CurrentQuestion = 0;

        //是否對題目打亂順序
        ShuffleQuestions();

        //是否對題目的數量有限制
        QuestionsLimit = GetQuestionLimit();

        //更新分數
        UpdateScore(true);

        //更新計時器
        UpdateTimer(true);

        //更新生命值
        //UpdateLives(true);

        //更新生命值
        UpdateQuestionNum(true);

        if (CurrentCategory.CanExitHalf)
        {
            StartCoroutine(myWWW.SelectUserRecord(userId, CurrentCategory.TestIndex));
        }

        isGameOver = Paused = false;

        CategoryName.text = CurrentCategory.CategoryName;

        /*if (CurrentCategory.Mode == DownloadMode.Online)
        {
            ImageLinks.Clear();
            FaultyLinks.Clear();

            for (int a = 0; a < QuestionList.Count; a++) {
                if (QuestionList[a].ExplainationImage.Length > 1)
                    ImageLinks.Add(QuestionList[a].ExplainationImage);
                if (QuestionList[a].QuestionImage.Length > 1)
                    ImageLinks.Add(QuestionList[a].QuestionImage);
                if (QuestionList[a].IsChoicewithImages) {
                    for (int b = 0; b < QuestionList[a].ChoiceDetails.Length; b++) {
                        ImageLinks.Add(QuestionList[a].ChoiceDetails[b].choice);
                    }
                }
             
             }

            //緩存圖片
            StartCoroutine(CacheImages());
        }*/

#if USE_DOTWEEN
        ResetScale(true);
#endif

        CategoriesCanvas.SetActive(false);
        GameOverCanvas.SetActive(false);
        PauseCanvas.SetActive(false);
        GameCanvas.SetActive(true);

        QuitGamePanel.SetActive(false);
        PostQuestionButtons.SetActive(false);
        ExplanationPanel.SetActive(false);

        isInGame = true;

        if (CurrentCategory.SpecialTemplate != CategoryFormat.TestTemplate.AutoVisualSearch && CurrentCategory.SpecialTemplate != CategoryFormat.TestTemplate.FrogMap && CurrentCategory.SpecialTemplate != CategoryFormat.TestTemplate.TOT && CurrentCategory.SpecialTemplate != CategoryFormat.TestTemplate.SDT)
        {
            StartCoroutine(LoadQuestion());
        }
        else {
            //主要問題標籤的顯示
            MainQuestionDisplay.gameObject.SetActive(false);
            //輔助問題標籤的顯示
            AltQuestionDisplay.gameObject.SetActive(false);
            //圖標展示區的顯示
            ImageDisplay.gameObject.SetActive(false);
            ImageParent.gameObject.SetActive(false);

            ExplanationAudio.gameObject.SetActive(false);

            //選擇題的按鈕顯示
            ToFAnswersParent.SetActive(false);
            //長問題按鈕的顯示
            AnswerBarsParent.SetActive(false);
            //短問題按鈕的顯示
            ShortAnswerBarsParent.SetActive(false);
            //錄音按鈕的顯示
            RecordingPage.SetActive(false);
            //繪畫按鈕的顯示
            DrawingPage.SetActive(false);
            //大圖選擇界面
            ChoosePicturePanel.SetActive(false);
            //圈畫的選擇界面
            MarkingPanel.SetActive(false);
            //判斷多個正誤的界面
            JudgePanel.SetActive(false);
            //青蛙地圖題目的界面
            FrogMap.SetActive(false);
            //找目標題目的界面
            AutoVsPanel.SetActive(false);
            //判斷真假的界面
            ToFAnswersParent.SetActive(false);
            //TOT測試的界面
            TOTPanel.SetActive(false);
            //SDT測試的界面
            SDTPanel.SetActive(false);

            QuestionNumText.color = Color.black;

            //設置問題回答為空
            Answered = false;
            //是否計時為否
            EnableTimer = false;

            cummulatedCorrect.Clear();
            cummulatedQueNum.Clear();

            if (CurrentCategory.SpecialTemplate == CategoryFormat.TestTemplate.PPTV) {
                cummulatedQueNum = new ArrayList();
                cummulatedQueIndex = 0;
                cummulatedCorrect = new ArrayList();
            }

            if (CurrentCategory.SpecialTemplate == CategoryFormat.TestTemplate.TOT) {
                TOTPanel.SetActive(true);
                TOTPanel.GetComponent<ToTController>().StartGame();
            }

            if (CurrentCategory.SpecialTemplate == CategoryFormat.TestTemplate.SDT)
            {
                SDTPanel.SetActive(true);
                SDTPanel.GetComponent<SDTController>().StartGame();
            }

            if (CurrentCategory.SpecialTemplate == CategoryFormat.TestTemplate.AutoVisualSearch)
            {
                AutoVsPanel.SetActive(true);         
                AutoVsPanel.GetComponent<AutoVisualSearch>().SetVSsettings(VSList.ToArray());           
                return;

            }else if (CurrentCategory.SpecialTemplate == CategoryFormat.TestTemplate.FrogMap)
            {
                FrogMap.SetActive(true);
                QuestionNumText.gameObject.SetActive(false);
                FrogMap.GetComponent<AutoFrogMap>().SetFrogSettings(FrogList.ToArray(), ordered);
                return;

            }

           
        }

       

        
    }

#if USE_DOTWEEN
    private void ResetScale(bool All = false)
    {
        if (All)
        {
            for (int x = 0; x < ToFAnswerHolder.Length; x++)
                ToFAnswerHolder[x].transform.localScale = Vector3.zero;
            for (int x = 0; x < AnswerParent.Length; x++)
                AnswerParent[x].transform.localScale = Vector3.zero;
            for (int x = 0; x < ShortAnswerParent.Length; x++)
                ShortAnswerParent[x].transform.localScale = Vector3.zero;

            return;
        }


            if (AspectRatio == AspectRatios.Default)
            {
                for (int x = 0; x < AnswerParent.Length; x++)
                    AnswerParent[x].transform.localScale = Vector3.zero;
            }
            else
            {
                for (int x = 0; x < ShortAnswerParent.Length; x++)
                    ShortAnswerParent[x].transform.localScale = Vector3.zero;
            }

    }
#endif
    /// <summary>
    /// 答題時間
    /// </summary>
    /// <returns></returns>
    private int GetTimerAmount()
    {
        if (CurrentCategory.CustomTimerAmount)
            return CurrentCategory.TimerAmount;
        else
            return UniversalTimerAmount;
    }

    /// <summary>
    /// 更新計時器
    /// </summary>
    /// <param name="init"></param>
    private void UpdateTimer(bool init = false)
    {
        if (EnableTimer)
        {
            if (init)
            {
                //Ticking = false;
                TimerText.gameObject.SetActive(true);
                TimeLeft = GetTimerAmount();
            }
            else
                TimeLeft--;

            TimerText.text = string.Concat(TimeLeft.ToString(), "\"");
        }
        else if (!EnableTimer && init)
            TimerText.gameObject.SetActive(false);
    }

    /// <summary>
    /// 生命值
    /// </summary>
    /// <returns></returns>
    private int GetLivesAmount()
    {
        if (CurrentCategory.CustomLivesAmount)
            return CurrentCategory.LivesCount;
        else
            return UniversalLivesAmount;
    }

    /// <summary>
    /// 調整生命值
    /// </summary>
    /// <param name="init"></param>
    private void UpdateLives(bool init = false)
    {
        if (EnableLives)
        {
            if (init)
            {
                LivesText.gameObject.SetActive(true);
                LivesLeft = GetLivesAmount();
            }
            else
            {
                LivesAnimation.Play();
            }

            LivesText.text = string.Concat("答題機會: ", LivesLeft.ToString());
        }
        else if (!EnableLives && init)
            LivesText.gameObject.SetActive(false);
    }

    public void UpdateQuestionNum(bool init = false)
    {
        if (init)
        {
            QuestionLeft = 0;
            TotalQuestionNum = 0;
            for (int i = 0; i < QuestionList.Count; i++)
            {
                if (QuestionList[i].Index != "")
                {
                    QuestionLeft++;
                }
            }

            TotalQuestionNum = QuestionLeft;
        }
        else
        {
            QuestionLeft--;
        }
        QuestionNumText.gameObject.SetActive(true);
        QuestionNumText.text = "答題進度: "+ (TotalQuestionNum-QuestionLeft)+"/"+TotalQuestionNum;
    }

    /// <summary>
    /// 更新分數
    /// </summary>
    /// <param name="init"></param>
    private void UpdateScore(bool init = false)
    {
        UpdateScore(0, init);
    }

    /// <summary>
    /// 更新分數
    /// </summary>
    /// <param name="increase"></param>
    /// <param name="init"></param>
    private void UpdateScore(int increase, bool init = false)
    {
        if (init)
            CurrentScore = 0;
        else
        {
            CurrentScore += increase;
            ScoreAnimation.Play();
        }

        ScoreText.text = string.Concat("Score: ", CurrentScore);
    }

    /// <summary>
    /// 獲得題目的限定數量
    /// </summary>
    /// <returns></returns>
    private int GetQuestionLimit()
    {
        if (CurrentCategory.LimitQuestions)
        {
            if (QuestionList.Count > CurrentCategory.QuestionLimit)
                return CurrentCategory.QuestionLimit - 1;
            else
                return QuestionList.Count - 1;
        }
        else
            return QuestionList.Count - 1;
    }

    //Shuffles the question list
    /// <summary>
    /// 打亂問題順序
    /// </summary>
    void ShuffleQuestions()
    {
        if (!CurrentCategory.ShuffleQuestions)
            return;

        for (int index = 0; index < QuestionList.Count; index++)
        {
            QuestionFormat tempNumber = QuestionList[index];

            int randomIndex = UnityEngine.Random.Range(index, QuestionList.Count);

            QuestionList[index] = QuestionList[randomIndex];

            QuestionList[randomIndex] = tempNumber;
        }
    }

    /// <summary>
    /// 打亂題目順序
    /// </summary>
    //Shuffles the answer list
    void ShuffleAnswers()
    {
        if (!CurrentCategory.ShuffleAnswers)
            return;

        for (int index = 0; index < AnswersList.Count; index++)
        {
            ChoiceDetailsFormat tempNumber = AnswersList[index];

            int randomIndex = UnityEngine.Random.Range(index, AnswersList.Count);

            AnswersList[index] = AnswersList[randomIndex];

            AnswersList[randomIndex] = tempNumber;
        }
    }

    /// <summary>
    /// 緩存圖片
    /// </summary>
    /// <returns></returns>
    private IEnumerator CacheImages()
    {
        if (ImageLinks.Count <= 0 || isGameOver)
            yield break;

        string EncodedString = CalculateMD5Hash(ImageLinks[0]);

        string filePath = Path.Combine(Application.temporaryCachePath, EncodedString);

        //Check if the image already exists
        if (!File.Exists(filePath))
        {
            //Initialize a new bool and set it to false. We will use this to retry a download in case it fails on the first try
            int retried = 0;

        //We will use this if we need to retry a download in case it fails on the first try
        TryAgain:

            //Start a new download from the provided URL
            using (UnityWebRequest CacheRequest = UnityWebRequestTexture.GetTexture(ImageLinks[0]))
            {
                //Reset the timeout amount to the default
                CacheRequest.timeout = DownloadTimeout;

                yield return CacheRequest.SendWebRequest();

                //Check if the downloaded successfully
                if (!CacheRequest.isNetworkError && !CacheRequest.isHttpError)
                {
                    Texture2D testTexture = DownloadHandlerTexture.GetContent(CacheRequest);

                    if (testTexture.width == 8 && testTexture.height == 8)
                    {
#if DEBUG
                        Debug.LogWarning("Unsupported image was downloaded. Deleting file...");
#endif
                        if (File.Exists(filePath))
                            File.Delete(filePath);

                        if (retried < 2)
                        {
                            yield return new WaitForSeconds(1);
                            retried++;
                            goto TryAgain;
                        }
                        else
                        {
                            FaultyLinks.Add(ImageLinks[0]);
                            ImageLinks.RemoveAt(0);
                            StartCoroutine(CacheImages());
                        }

                    }
                    else
                    {
                        File.WriteAllBytes(filePath, CacheRequest.downloadHandler.data);
                        ImageLinks.RemoveAt(0);
                        StartCoroutine(CacheImages());
                    }

                    Destroy(testTexture);
                }
                else
                {
                    //If the download failed, retry again in x seconds if we haven't retried it before
                    if (retried < 2)
                    {
                        yield return new WaitForSeconds(1);
                        retried++;
                        goto TryAgain;
                    }
                    else
                    {
                        FaultyLinks.Add(ImageLinks[0]);
                        ImageLinks.RemoveAt(0);
                        StartCoroutine(CacheImages());
                    }
                }
            }
        }
        else
        {
            ImageLinks.RemoveAt(0);
            StartCoroutine(CacheImages());
        }
    }

    private string CalculateMD5Hash(string input)
    {
        UTF8Encoding ue = new UTF8Encoding();
        byte[] bytes = ue.GetBytes(input);

        MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < hashBytes.Length; i++)
        {
            sb.Append(hashBytes[i].ToString("x2"));
        }

        return sb.ToString();
    }

    /// <summary>
    /// 加載題目
    /// </summary>
    /// <returns></returns>
    private IEnumerator LoadQuestion()
    {
        if (UserRecord.Count > 0 && QuestionList[CurrentQuestion].Index!="") {
            //判断是不是答过的题目
            foreach (User_Record userrecord in UserRecord)
            {
                Debug.Log("QuestionList[CurrentQuestion].Index:" + QuestionList[CurrentQuestion].Index);
                if (userrecord.questionIndex == int.Parse(QuestionList[CurrentQuestion].Index))
                {
                     
                    CurrentQuestion++;
                    UpdateQuestionNum();
                    StartCoroutine(LoadQuestion());
                    yield break;
                }
            }
        }

        //判斷當前的練習要不要跳過
        if (UserRecord.Count > 0 && QuestionList[CurrentQuestion].Index == "") {
            int tmpquestion = CurrentQuestion;
            do
            {
                tmpquestion++;
                Debug.Log("QuestionList[tmpquestion].Index:" + QuestionList[tmpquestion].Index);
            } while (QuestionList[tmpquestion].Index == "");

            foreach (User_Record userrecord in UserRecord)
            {           
                if (userrecord.questionIndex == int.Parse(QuestionList[tmpquestion].Index))
                {
                        CurrentQuestion = tmpquestion;
                        StartCoroutine(LoadQuestion());
                        yield break;
                }
            }
            
        }
        

        //主要問題標籤的顯示
        MainQuestionDisplay.gameObject.SetActive(false);
        //輔助問題標籤的顯示
        AltQuestionDisplay.gameObject.SetActive(false);
        //圖標展示區的顯示
        ImageDisplay.gameObject.SetActive(false);
        ImageParent.gameObject.SetActive(false);

        ExplanationAudio.gameObject.SetActive(false);

        //選擇題的按鈕顯示
        ToFAnswersParent.SetActive(false);
        //長問題按鈕的顯示
        AnswerBarsParent.SetActive(false);
        //短問題按鈕的顯示
        ShortAnswerBarsParent.SetActive(false);
        //錄音按鈕的顯示
        RecordingPage.SetActive(false);
        //繪畫按鈕的顯示
        DrawingPage.SetActive(false);
        //大圖選擇界面
        ChoosePicturePanel.SetActive(false);
        //圈畫的選擇界面
        MarkingPanel.SetActive(false);
        //判斷多個正誤的界面
        JudgePanel.SetActive(false);
        //青蛙地圖題目的界面
        FrogMap.SetActive(false);
        //找目標題目的界面
        AutoVsPanel.SetActive(false);
        //判斷真假的界面
        ToFAnswersParent.SetActive(false);
        //TOT測試的界面
        TOTPanel.SetActive(false);
        //SDT測試的界面
        SDTPanel.SetActive(false);

        //設置問題回答為空
        Answered = false;
        //是否計時為否
        EnableTimer = false;

        //緩存圖片
        /*if (CurrentCategory.Mode == DownloadMode.Online)
        {
            if (QuestionList[CurrentQuestion].QuestionImage.Length > 1)
            {
                string EncodedString = CalculateMD5Hash(QuestionList[CurrentQuestion].QuestionImage);
                string filePath = Path.Combine(Application.temporaryCachePath, EncodedString);

            TryAgain:

                WaitingForImage = false;

                //Check if the image already exists
                if (File.Exists(filePath))
                {
                    //Load the image from the phone storage
                    using (UnityWebRequest ImageRequest = UnityWebRequestTexture.GetTexture("file:///" + filePath))
                    {

                        //Wait for the image to load successfully
                        yield return ImageRequest.SendWebRequest();

                        if (ImageRequest.isHttpError || ImageRequest.isNetworkError)
                        {
#if DEBUG
                            Debug.Log("Image Request error - " + ImageRequest.error);
#endif
                            Continue();
                            yield break;
                        }

                        Texture2D imgTex = DownloadHandlerTexture.GetContent(ImageRequest);

                        //Load the sprite to the image display
                        ImageDisplay.texture = imgTex;

                        ImageAspectRatioFitter.aspectRatio = (float)imgTex.width / (float)imgTex.height;
                    }
                }
                else
                {
                    int timer = 0;

                    ImageDownloadAnimation.gameObject.SetActive(true);
                    ImageDownloadAnimation.Play();

                    WaitingForImage = true;

                    while (!File.Exists(filePath))
                    {
                        yield return new WaitForSeconds(1f);


                        timer++;

                        for (int count = 0; count < FaultyLinks.Count; count++)
                        {
                            if (QuestionList[CurrentQuestion].QuestionImage.Equals(FaultyLinks[count]))
                            {
                                ImageDownloadAnimation.Stop();
                                ImageDownloadAnimation.gameObject.SetActive(false);
                                Continue();
                                yield break;
                            }
                        }

                        if (timer >= DownloadTimeout)
                        {
                            ImageDownloadAnimation.Stop();
                            ImageDownloadAnimation.gameObject.SetActive(false);
                            Continue();
                            yield break;
                        }
                    }

                    ImageDownloadAnimation.Stop();
                    ImageDownloadAnimation.gameObject.SetActive(false);

                    goto TryAgain;
                }

                AltQuestionDisplay.text = QuestionList[CurrentQuestion].Question;
                AltQuestionDisplay.gameObject.SetActive(true);
                ImageDisplay.gameObject.SetActive(true);
                ImageParent.gameObject.SetActive(true);
            }
            else
            {
                MainQuestionDisplay.text = QuestionList[CurrentQuestion].Question;
                MainQuestionDisplay.gameObject.SetActive(true);
            }
        }
        else*/
        SetupQuestion();//顯示題目

        /*if (QuestionList[CurrentQuestion].isToF)
        {
            ToFAnswersHeading.gameObject.SetActive(false);

            ToFAnswerHolder[0].SetActive(true);
            ToFAnswerHolder[1].SetActive(true);

            ToFAnswersParent.SetActive(true);

            ToFAnswerHoldersCanvasGroup.blocksRaycasts = true;

            ToFQuestion = true;
            RecordingQuestion = false;
            DrawingQuestion = false;
        }
        else*/



        //判斷是不是指導語
        if (QuestionList[CurrentQuestion].IsInstruction) {
            QuestionNumText.gameObject.SetActive(false);
            StartCoroutine(PostQuestion());
            yield break;
        }




        QuestionNumText.gameObject.SetActive(true);
        //是文字選擇題
        if (QuestionList[CurrentQuestion].IsChoicewithWord)
        {
            Debug.Log("是文字選擇題呀");
            AnswersList.Clear();
            for (int x = 0; x < QuestionList[CurrentQuestion].ChoiceDetails.Length; x++)
                AnswersList.Add(QuestionList[CurrentQuestion].ChoiceDetails[x]);

            //ShuffleAnswers();

            for (int count = 0; count < AnswerDisplay.Length; count++)
            {
                if (count >= AnswersList.Count)
                {
                 AnswerParent[count].gameObject.SetActive(false);
                 continue;
                }

                 AnswerParent[count].color = DefaultColor;
                 AnswerParent[count].gameObject.SetActive(true);
                AnswerParent[count].transform.localScale = new Vector3(1, 1, 0);
                AnswerDisplay[count].text = AnswersList[count].choice;
               
            }

            AnswerBarsParent.SetActive(true);
            if (QuestionList[CurrentQuestion].QuestionAudio != "") {
                ExplanationAudio.gameObject.SetActive(true);
                PlaySound(QuestionList[CurrentQuestion].QuestionAudio.Split('.')[0]);
            }
            AnswerBarsCanvasGroup.blocksRaycasts = true;

            RecordingQuestion = false;
            DrawingQuestion = false;
           
        }

        //是小圖片選擇題
        if (QuestionList[CurrentQuestion].IsChoicewithImages && (QuestionList[CurrentQuestion].Question!=""|| QuestionList[CurrentQuestion].QuestionImage!="")) {
            AnswersList.Clear();
            Debug.Log("小圖片選擇題呀");
            for (int x = 0; x < QuestionList[CurrentQuestion].ChoiceDetails.Length; x++)
                AnswersList.Add(QuestionList[CurrentQuestion].ChoiceDetails[x]);

            ShuffleAnswers();

            for (int count = 0; count < ShortAnswerDisplay.Length; count++)
            {
                if (count >= AnswersList.Count)
                {
                    ShortAnswerParent[count].gameObject.SetActive(false);
                    continue;
                }

                ShortAnswerParent[count].color = DefaultColor;
                ShortAnswerParent[count].gameObject.SetActive(true);
                ShortAnswerParent[count].transform.localScale = new Vector3(0.8f, 1, 1);
                Debug.Log("Images/" + AnswersList[count].choice.Split('.')[0]);
                ShortAnswerParent[count].GetComponent<Image>().sprite = Resources.Load<Sprite>("Images/" + AnswersList[count].choice.Split('.')[0]);
                ShortAnswerDisplay[count].text = "";

                ShortAnswerBarsParent.SetActive(true);
                if (QuestionList[CurrentQuestion].QuestionAudio != "")
                {
                    ExplanationAudio.gameObject.SetActive(true);
                    PlaySound(QuestionList[CurrentQuestion].QuestionAudio.Split('.')[0]);
                }
                ShortAnswerBarsCanvasGroup.blocksRaycasts = true;
            }

            RecordingQuestion = false;
            DrawingQuestion = false;           
        }

        //是大圖片選擇題
        if (QuestionList[CurrentQuestion].IsChoicewithImages && QuestionList[CurrentQuestion].Question == "" && QuestionList[CurrentQuestion].QuestionImage== "")
        {
            AnswersList.Clear();
            Debug.Log("是大圖片選擇題呀");
            for (int x = 0; x < QuestionList[CurrentQuestion].ChoiceDetails.Length; x++)
                AnswersList.Add(QuestionList[CurrentQuestion].ChoiceDetails[x]);


            for (int count = 0; count < PictureAnswerParent.Length; count++)
            {
                if (count >= AnswersList.Count)
                {
                    PictureAnswerParent[count].gameObject.SetActive(false);
                    continue;
                }

                PictureAnswerParent[count].color = DefaultColor;
                PictureAnswerParent[count].gameObject.SetActive(true);
                PictureAnswerParent[count].transform.localScale = new Vector3(1f, 1, 1);

                PictureAnswerParent[count].GetComponent<Image>().sprite = Resources.Load<Sprite>("Images/" + AnswersList[count].choice.Split('.')[0]);

                ChoosePicturePanel.SetActive(true);
                if (QuestionList[CurrentQuestion].QuestionAudio != "")
                {
                    ExplanationAudio.gameObject.SetActive(true);
                   
                }
                PictureAnswerBarsCanvasGroup.blocksRaycasts = true;
            }

            PlaySound(QuestionList[CurrentQuestion].QuestionAudio.Split('.')[0]);

            RecordingQuestion = false;
            DrawingQuestion = false;
        }

        //是繪畫題
        if (QuestionList[CurrentQuestion].IsDrawing)
        {
            Debug.Log("是繪畫題");
            RecordingQuestion = false;
            DrawingQuestion = true;
            DrawingPage.SetActive(true);
            DrawingHolderGroup.blocksRaycasts = true;
            if (QuestionList[CurrentQuestion].QuestionAudio != "")
            {
                ExplanationAudio.gameObject.SetActive(true);
                PlaySound(QuestionList[CurrentQuestion].QuestionAudio.Split('.')[0]);
            }
            
        }

        //是錄音題
        if (QuestionList[CurrentQuestion].IsRecording)
        {

            RecordingQuestion = true;
            DrawingQuestion = false;
            recorder.Init();
            recorderEmpty = true;
            EnableRecorder();
            RecordingPage.SetActive(true);
            RecordingHolderGroup.blocksRaycasts = true;
            StartBtn.transform.Find("Text").GetComponent<Text>().text = "開始錄音答題";
            if (QuestionList[CurrentQuestion].QuestionAudio != "")
            {
                ExplanationAudio.gameObject.SetActive(true);
                PlaySound(QuestionList[CurrentQuestion].QuestionAudio.Split('.')[0]);
            }     
        }

        //是圈圖題
        if (QuestionList[CurrentQuestion].IsMarking) {
            Debug.Log("是圈圖題");
            MarkingPanel.SetActive(true);
            int row = int.Parse(QuestionList[CurrentQuestion].QuestionSliced.Split('*')[0]);
            int column = int.Parse(QuestionList[CurrentQuestion].QuestionSliced.Split('*')[1]);
            myMarkChoice.PrepareMarkChoice(row, column, QuestionList[CurrentQuestion].QuestionImage,QuestionList[CurrentQuestion].Question);
            
            if (QuestionList[CurrentQuestion].QuestionAudio != "")
            {
                ExplanationAudio.gameObject.SetActive(true);
                PlaySound(QuestionList[CurrentQuestion].QuestionAudio.Split('.')[0]);
            }
        }

        //是多圖判斷題
        if (QuestionList[CurrentQuestion].IsTellImage)
        {
            Debug.Log("題目類型：真假字判斷");
            JudgePanel.SetActive(true);
            MyJudgeController.PrepareGame(QuestionList[CurrentQuestion].QuestionImage);
            if (QuestionList[CurrentQuestion].QuestionAudio != "")
            {
                ExplanationAudio.gameObject.SetActive(true);
                PlaySound(QuestionList[CurrentQuestion].QuestionAudio.Split('.')[0]);
            }
        }

        //是正確錯誤題
        if (QuestionList[CurrentQuestion].IsTrueorFalse) {
            Debug.Log("題目類型：真假字判斷");
            ToFAnswersParent.SetActive(true);
            ToFAnswerHoldersCanvasGroup.blocksRaycasts = true;
            MyJudgeController.PrepareGame(QuestionList[CurrentQuestion].QuestionImage);
            if (QuestionList[CurrentQuestion].QuestionAudio != "")
            {
                ExplanationAudio.gameObject.SetActive(true);
                PlaySound(QuestionList[CurrentQuestion].QuestionAudio.Split('.')[0]);
            }
        }

        //判斷是不是要顯示計時
        if (QuestionList[CurrentQuestion].TimeLimited.Trim() != "")
        {
            EnableTimer = true;
            TimeLeft = int.Parse(QuestionList[CurrentQuestion].TimeLimited);
            TimerText.gameObject.SetActive(true);
            TimerText.text = string.Concat(TimeLeft.ToString(), "\"");
        }

        //如果不是這種情況則直接計時
        if(!(QuestionList[CurrentQuestion].IsRecording && QuestionList[CurrentQuestion].QuestionImage != ""&&QuestionList[CurrentQuestion].RecordingLimited!="") && QuestionList[CurrentQuestion].QuestionAudio=="") {
            //圖片+錄音題 -> 點擊錄音才開始倒計時
            //其它題 -> 錄音播放完開始計時
           
            if(EnableTimer)
                StartCoroutine(StartTimer());
            

        }

        if (QuestionList[CurrentQuestion].RecordingLimited != "")
        {
            LivesLeft = int.Parse(QuestionList[CurrentQuestion].RecordingLimited);
            LivesText.text = string.Concat("答題機會: ", LivesLeft.ToString());
            LivesText.gameObject.SetActive(true);

        }
        else {
            LivesText.gameObject.SetActive(false);
        }


        if (QuestionList[CurrentQuestion].IsTrial)
        {
            QuestionNumText.text = "答題進度：練習階段 ";
        }
        else {
            if (TotalQuestionNum - QuestionLeft == 0)
            {
                QuestionNumText.text = "答題進度：0/" + TotalQuestionNum;
            }
            else {
                QuestionNumText.text = "答題進度："+ (TotalQuestionNum - QuestionLeft) + "/" + TotalQuestionNum;
            }
        }

        startTimeStamp = GetTimeStamp();


    }

    private void SetupQuestion()
    {
        if (QuestionList[CurrentQuestion].QuestionImage.Contains(",") || QuestionList[CurrentQuestion].IsMarking) {
            return;
        }

        //設置圖片大小
        if (QuestionList[CurrentQuestion].QuestionImageSizeType.ToString() != "") {
            int imageWidth = int.Parse(QuestionList[CurrentQuestion].QuestionImageSizeType.ToString().Split('*')[0]);
            int imageHeight = int.Parse(QuestionList[CurrentQuestion].QuestionImageSizeType.ToString().Split('*')[1]);

            ImageParent.GetComponent<RectTransform>().sizeDelta = new Vector2(imageWidth, imageHeight);
        }
        

        if (QuestionList[CurrentQuestion].QuestionImage.Length > 1)
        {
            Texture2D imgTex = Resources.Load<Texture2D>("Images/" + QuestionList[CurrentQuestion].QuestionImage.Split('.')[0]);


            ImageDisplay.texture = imgTex;

            ImageAspectRatioFitter.aspectRatio = (float)imgTex.width / (float)imgTex.height;
            AltQuestionDisplay.text = QuestionList[CurrentQuestion].Question;
            AltQuestionDisplay.gameObject.SetActive(true);

            if (QuestionList[CurrentQuestion].RecordingLimited != "" && QuestionList[CurrentQuestion].QuestionImage != "" && QuestionList[CurrentQuestion].IsRecording)
            {
                
            }
            else {
                ImageDisplay.gameObject.SetActive(true);
                ImageParent.gameObject.SetActive(true);
            }
                
        }
        else
        {
            MainQuestionDisplay.text = QuestionList[CurrentQuestion].Question;
            MainQuestionDisplay.gameObject.SetActive(true);
        }
    }

#if UNITY_EDITOR
    /*private void AutoAnswer()
    {
        //是選擇題(暫時不用管)
        if (QuestionList[CurrentQuestion].IsChoicewithImages || QuestionList[CurrentQuestion].IsChoicewithWord) {

        }
        //是錄音題
        if (QuestionList[CurrentQuestion].IsRecording) {
            recorder.StopRecording();
            //Btn_SaveRecording();
        }
        //是繪畫題(暫時不用管)
        if (QuestionList[CurrentQuestion].IsDrawing) {

        }

        /*if (QuestionList[CurrentQuestion].isToF)
        {
            ToFAnswer(QuestionList[CurrentQuestion].ToFAnswer);
        }
        else
        {
            for (int x = 0; x < AnswersList.Count; x++)
                if (AnswersList[x].Correct)
                {
                    MultipleChoiceAnswer(x);
                    return;
                }
        }
    }*/
#endif

#if USE_DOTWEEN
    private IEnumerator AnimateButtons()
    {


            if (AspectRatio == AspectRatios.Default)
            {
                for (int x = 0; x < AnswerParent.Length; x++)
                {
                    if (AnswerParent[x].gameObject.activeInHierarchy)
                    {
                        AnswerParent[x].transform.DOScale(Vector3.one, 0.15f);
                        yield return new WaitForSeconds(0.1f);
                    }
                }
            }
            else
            {
                for (int x = 0; x < ShortAnswerParent.Length; x++)
                {
                    if (ShortAnswerParent[x].gameObject.activeInHierarchy)
                    {
                        ShortAnswerParent[x].transform.DOScale(Vector3.one, 0.15f);
                        yield return new WaitForSeconds(0.1f);
                    }
                }
            }

        StartCoroutine(StartTimer());
    }
#endif

    private IEnumerator StartTimer()
    {       
        if (!EnableTimer)
            yield break;

        Debug.Log("come to timer");
        while (TimeLeft > 0 && !Answered)
        {
            yield return new WaitForSeconds(1);

            if (!Answered)
                UpdateTimer();
        }

        //如果到時間沒有人回答
        if (!Answered)
        {
            if (QuestionList[CurrentQuestion].IsChoicewithImages || QuestionList[CurrentQuestion].IsChoicewithWord)
            {
                MultipleChoiceAnswer(-1);

            } else if (QuestionList[CurrentQuestion].IsDrawing) {

                Btn_SaveDrawing();

            } else if (QuestionList[CurrentQuestion].IsRecording) {

                //正在錄製中
                if (recorder.IsRecording)
                {
                    Btn_Recording();//點擊暫停
                    SaveBtn.SetActive(false);
                    Btn_SaveRecording();
                }
                else if (recorder.hasRecorded && recorderEmpty == false)
                { //已經錄製完
                    SaveBtn.SetActive(false);
                    Btn_SaveRecording();
                }
                else {//未開始錄製
                    Btn_SaveRecording(false);
                }
                
                

            } else
            {
                //HandleAnswer(false, 0, true);

                AnswerBarsCanvasGroup.blocksRaycasts = false;
            }
        }

        if (isGameOver || Paused)
        {
            if (Ticking)
            {
                StopAudioPlayer();
                Ticking = false;
            }
        }
    }



    public void ToFAnswer(bool isTrue)
    {
        int selected = 0;

        if (!isTrue)
            selected = 1;

        HandleAnswer(false, selected);

        ToFAnswerHoldersCanvasGroup.blocksRaycasts = false;
    }

    public void MultipleChoiceAnswer(int selected)
    {
        Answered = true;

        if (selected == -1) {
            HandleAnswer(false, selected);
            return;
        }

        //判斷有有沒有選中
        if (AnswerParent[selected].color == ChoosenColor || ShortAnswerParent[selected].color == ChoosenColor || PictureAnswerParent[selected].color == ChoosenColor)
        {
            HandleAnswer(AnswersList[selected].correct.ToString() == "1", selected);
            Debug.Log("enter multiplechoiceAns");

            //if (AspectRatio == AspectRatios.Default)
                AnswerBarsCanvasGroup.blocksRaycasts = false;
            //else
                ShortAnswerBarsCanvasGroup.blocksRaycasts = false;
                PictureAnswerBarsCanvasGroup.blocksRaycasts = false;
        }
        else {
            if (QuestionList[CurrentQuestion].IsChoicewithWord)
            {
                //其它的清理掉
                foreach (Image child in AnswerParent)
                {
                    child.color = DefaultColor;
                }

                AnswerParent[selected].color = ChoosenColor;
            }
            else if (QuestionList[CurrentQuestion].IsChoicewithImages&&(QuestionList[CurrentQuestion].QuestionImage!=""||QuestionList[CurrentQuestion].Question!=""))
            {
                //其它的清理掉
                foreach (Image child in ShortAnswerParent)
                {
                    child.color = DefaultColor;
                }

                ShortAnswerParent[selected].color = ChoosenColor;
            }
            else {
                //其它的清理掉
                foreach (Image child in PictureAnswerParent)
                {
                    child.color = DefaultColor;
                }

                PictureAnswerParent[selected].color = ChoosenColor;
            }
        }
        
       
    }

    
    private void HandleAnswer(bool Correct = false, int selected = 0, bool NoResponse = false)
    {
        Answered = true;
        StopAudioPlayer();

        PlayButtonClick();

        if (Ticking)
        {
            StopAudioPlayer();
            Ticking = false;
        }

        if (selected != -1) {
            //變色
            if (QuestionList[CurrentQuestion].IsChoicewithWord)
            {
                AnswerParent[selected].color = CorrectColor;
            }
            else if (QuestionList[CurrentQuestion].IsChoicewithImages && (QuestionList[CurrentQuestion].QuestionImage != "" || QuestionList[CurrentQuestion].Question != ""))
            {
                ShortAnswerParent[selected].color = CorrectColor;
            }
            else {
                PictureAnswerParent[selected].color = CorrectColor;
            }
        }

        
        
       


        if (!QuestionList[CurrentQuestion].IsTrial)
        {
            string filename = QuestionList[CurrentQuestion].Index + "_" + userId;
            //保存到數據庫
            endTimeStamp = GetTimeStamp();
            //专门给PPTV21使用的
            if (QuestionList[CurrentQuestion].TextIndex == 21)
            {
                myWWW.SaveToDatabase(userId, int.Parse(QuestionList[CurrentQuestion].Index), selected.ToString() + "_" + Correct, startTimeStamp, endTimeStamp);
            }
            else {
                myWWW.SaveToDatabase(userId, int.Parse(QuestionList[CurrentQuestion].Index), selected.ToString(), startTimeStamp, endTimeStamp);
            }
            

            //調整題目數量
            UpdateQuestionNum();
            //专门给PPTV21使用的
            if (QuestionList[CurrentQuestion].TextIndex == 21)
            {
                Debug.Log("Correct:" + Correct);
                    cummulatedQueNum.Add(cummulatedQueIndex);
                    cummulatedCorrect.Add(Correct);
                    cummulatedQueIndex++;
                Debug.Log("cummulatedQueIndex:" + cummulatedQueIndex);
                Debug.Log("cummulatedCorrect:"+cummulatedCorrect.ToString());
                Debug.Log("cummulatedQueNum:" + cummulatedQueNum.ToString());

                if (cummulatedQueNum.Count == 8) {
                    int wrongnum = 0;
                    foreach (bool tmp in cummulatedCorrect) {
                        if (!tmp) {
                            wrongnum++;
                        }
                    }
                    if (wrongnum == 6)
                    {
                        string str = hasFinished;
                        if (str == "")
                        {
                            str = QuestionList[CurrentQuestion].TextIndex.ToString();
                        }
                        else
                        {
                            str = str + "," + QuestionList[CurrentQuestion].TextIndex.ToString();
                        }

                        hasFinished = str;
                        PlayerPrefs.SetString("hasFinished", str);

                        //保存到數據庫
                        StartCoroutine(myWWW.UpdateUser());
                        GameOver();
                        return;
                    }
                    else {
                        cummulatedQueNum.RemoveAt(0);
                        cummulatedCorrect.RemoveAt(0);
                        cummulatedQueIndex--;
                    }

                    Debug.Log("wrongnum:"+ wrongnum);
                }

              
            }
        }
        
           
        

        

        //正確與否的反應
        /* if (Correct)
         {
             PlaySound(CorrectSound);

             if (ToFQuestion)
             {
                 ToFAnswersHeading.text = "You are correct";
                 ToFAnswersHeading.gameObject.SetActive(true);

                 ToggleToFButtons(selected, true);
             }
             else
             {
                 if (AspectRatio == AspectRatios.Default)
                 {
                     for (int x = 0; x < AnswersList.Count; x++)
                         if (AnswersList[x].correct == "true")
                             AnswerParent[x].color = CorrectColor;
                 }
                 else
                 {
                     for (int x = 0; x < AnswersList.Count; x++)
                         if (AnswersList[x].correct == "true")
                             ShortAnswerParent[x].color = CorrectColor;
                 }
             }

             if (EnableTimer)
                 UpdateScore(PointsPerSecond * TimeLeft);
             else
                 UpdateScore(PointsPerQuestion);
         }
         else
         {
             //LivesLeft--;
             //UpdateLives();

             PlaySound(WrongSound);

 #if (SUPPORTED_PLATFORM || UNITY_EDITOR)
             if (VibrationEnabled)
                 Handheld.Vibrate();
 #endif

             if (ToFQuestion)
             {
                 ToFAnswersHeading.text = "The Correct Answer is";
                 ToFAnswersHeading.gameObject.SetActive(true);

                 ToggleToFButtons(selected, false);
             }
             else
             {
                 if (AspectRatio == AspectRatios.Default)
                 {
                     if (!NoResponse)
                         AnswerParent[selected].color = WrongColor;

                     for (int x = 0; x < AnswersList.Count; x++)
                         if (AnswersList[x].correct == "true")
                             AnswerParent[x].color = CorrectColor;
                 }
                 else
                 {
                     if (!NoResponse)
                         ShortAnswerParent[selected].color = WrongColor;

                     for (int x = 0; x < AnswersList.Count; x++)
                         if (AnswersList[x].correct == "true")
                             ShortAnswerParent[x].color = CorrectColor;
                 }
             }
         }*/


        StartCoroutine(PostQuestion());
    }

    private void ToggleToFButtons(int current, bool show)
    {
        if (current == 0)
        {
            if (show)
            {
                ToFAnswerHolder[0].SetActive(true);
                ToFAnswerHolder[1].SetActive(false);
            }
            else
            {
                ToFAnswerHolder[0].SetActive(false);
                ToFAnswerHolder[1].SetActive(true);
            }
        }
        else
        {
            if (show)
            {
                ToFAnswerHolder[0].SetActive(false);
                ToFAnswerHolder[1].SetActive(true);
            }
            else
            {
                ToFAnswerHolder[0].SetActive(true);
                ToFAnswerHolder[1].SetActive(false);
            }
        }
    }

    public void StartPostQuestion() {
        StartCoroutine(PostQuestion());
    }

    /// <summary>
    /// 顯示解釋
    /// </summary>
    /// <returns></returns>
    private IEnumerator PostQuestion()
    {
        yield return new WaitForSeconds(PostQuestionDelay);

        ExplanationImage.gameObject.SetActive(false);
        ExplanationAudio.gameObject.SetActive(false);
        ExplanationText.gameObject.SetActive(false);

        TimerText.gameObject.SetActive(false);
        LivesText.gameObject.SetActive(false);

        //關閉題目內容
        MainQuestionDisplay.gameObject.SetActive(false);
        ImageDisplay.gameObject.SetActive(false);
        ImageParent.gameObject.SetActive(false);
        AltQuestionDisplay.gameObject.SetActive(false);
        AutoVsPanel.SetActive(false);
        ChoosePicturePanel.SetActive(false);
        JudgePanel.SetActive(false);
        MarkingPanel.SetActive(false);
        FrogMap.SetActive(false);     

        if (QuestionList[CurrentQuestion].ExplainationImage != "")
        {
            //修改图片大小
            if (QuestionList[CurrentQuestion].QuestionImageSizeType.ToString() != "")
            {
                int imageWidth = int.Parse(QuestionList[CurrentQuestion].QuestionImageSizeType.ToString().Split('*')[0]);
                int imageHeight = int.Parse(QuestionList[CurrentQuestion].QuestionImageSizeType.ToString().Split('*')[1]);

                ExplanationImage.GetComponent<RectTransform>().sizeDelta = new Vector2(imageWidth, imageHeight);
            }
            Texture2D imgTex = Resources.Load<Texture2D>("Images/" + QuestionList[CurrentQuestion].ExplainationImage.Split('.')[0]);

            ExplanationImage.texture = imgTex;
            ExplanationImage.gameObject.SetActive(true);
        }

        if (QuestionList[CurrentQuestion].ExplainationAudio != "")
        {
            ExplanationAudio.gameObject.SetActive(true);
            PlaySound(QuestionList[CurrentQuestion].ExplainationAudio.Split('.')[0]);
        }

        if (QuestionList[CurrentQuestion].ExplainationText != "")
        {
            ExplanationText.gameObject.SetActive(true);
            ExplanationText.text = QuestionList[CurrentQuestion].ExplainationText;

        }



        if (ShowPostQuestionButtons && (QuestionList[CurrentQuestion].ExplainationImage != "" || QuestionList[CurrentQuestion].ExplainationAudio != "" || QuestionList[CurrentQuestion].ExplainationText != ""))
        {
            Resources.UnloadUnusedAssets();
            ExtraLifeButton.SetActive(false);
            PostQuestionButtons.SetActive(true);
            ToFAnswersParent.SetActive(false);
            AnswerBarsParent.SetActive(false);
            ShortAnswerBarsParent.SetActive(false);
            DrawingPage.SetActive(false);
            RecordingPage.SetActive(false);
        }
        else
            Continue();
    }

    public void ToggleExplanationPanel(bool open)
    {
        if (!open)
        {
            ExplanationPanel.SetActive(false);
            return;
        }

        ExplanationDisplay.text = QuestionList[CurrentQuestion].ExplainationText;
        ExplanationPanel.SetActive(true);
    }

    //initiate  recorder
    void EnableRecorder() {
        Recorder.onInit += OnInit;
        Recorder.onFinish += OnRecordingFinish;
        Recorder.onError += OnError;
        Recorder.onSaved += OnRecordingSaved;

        StartBtn.SetActive(true);
        PlayBtn.SetActive(false);
        SaveBtn.SetActive(false);
    }

    //disable recorder
    void DisableRecorder() {
        Recorder.onInit -= OnInit;
        Recorder.onFinish -= OnRecordingFinish;
        Recorder.onError -= OnError;
        Recorder.onSaved -= OnRecordingSaved;


        
       
    }

    //初始化
    void OnInit(bool success) {

        Debug.Log("recorderInitiatedSuccess:" + success);
    }

    //報錯
    void OnError(string errMsg) {
        Debug.Log("Error : " + errMsg);
    }
    //錄製完成以後播放
    void OnRecordingFinish(AudioClip clip)
    {
        if (autoPlay)
        {
            recorder.PlayAudio(clip, AudioPlayer);

            // or you can use
            //recorder.PlayRecorded(audioSource);
        }


    }

    //錄音被保存
    void OnRecordingSaved(string path)
    {


        //recorder.PlayAudio(path, AudioPlayer);
    }

    //record the voice
    public void Btn_Recording() {
       
        
        if (!recorder.IsRecording)
        {
            StartBtn.transform.Find("Text").GetComponent<Text>().text = "錄音中...";
            recorder.StartRecording(false, 60);
            PlayBtn.SetActive(false);
            SaveBtn.SetActive(false);
            if (QuestionList[CurrentQuestion].QuestionImage != "")
            {
                ImageDisplay.gameObject.SetActive(true);
                ImageParent.gameObject.SetActive(true);

                if (QuestionList[CurrentQuestion].Question.Split('：')[0] == "提醒" || QuestionList[CurrentQuestion].Question.Split(':')[0] == "提醒") {
                    AltQuestionDisplay.gameObject.SetActive(false);
                }
                
                if (QuestionList[CurrentQuestion].QuestionImage != "" && QuestionList[CurrentQuestion].RecordingLimited != "")
                {   
                    StartCoroutine(StartTimer());
                    Debug.Log("startTimer:Btn_Recording");
                }  
            }

            if (QuestionList[CurrentQuestion].TextIndex == 10)
            {
                StartBtn.gameObject.GetComponent<Button>().interactable = false;
            }
        }
        else {
            StartBtn.transform.Find("Text").GetComponent<Text>().text = "重 答";
            recorder.StopRecording();
            recorderEmpty = false;


            if (recorder.hasRecorded )
            {
                PlayBtn.SetActive(true);
                SaveBtn.SetActive(true);

            }
            else {
                PlayBtn.SetActive(true);
                SaveBtn.SetActive(true);
                Debug.Log("no record");
            }

            //如果只能錄製一次

            if (QuestionList[CurrentQuestion].RecordingLimited != "")
            {
                if (LivesLeft == 1)
                {
                    PlayBtn.SetActive(false);
                    StartBtn.SetActive(false);
                    Answered = true;
                    
                }
                else
                {
                    LivesLeft--;
                    LivesText.text = string.Concat("答題機會: ", LivesLeft.ToString());
                }

            }

        }
       
    }

    //play the voice
    public void Btn_PlayRecording() {
        //停止錄音
        recorder.PlayRecorded(AudioPlayer);
        Debug.Log("play recoring");

    }

    //save the voice
    public void Btn_SaveRecording(bool hasrecorder = true)
    {
        Answered = true;
        RecordingHolderGroup.blocksRaycasts = false;
        string filename = "";
        if (QuestionList[CurrentQuestion].Index != "") {
            if (hasrecorder)
            {
                //保存按鈕
                string location = string.Concat(Application.persistentDataPath, "/Audio" + UnityEngine.Random.Range(0, 10000) + ".wav");

                filename = QuestionList[CurrentQuestion].Index + "_" + userId + "_" + GetTimeStamp() + ".mp3";
                recorder.Save(location, recorder.Clip);
                //把圖片保存到服務器        
                myWWW.UploadMedia(2, filename, null, location);
            }
            else {
                filename = "no recorder";
            }
      
            //保存到數據庫
            endTimeStamp = GetTimeStamp();
            myWWW.SaveToDatabase(userId, int.Parse(QuestionList[CurrentQuestion].Index), filename,startTimeStamp, endTimeStamp);
            UpdateQuestionNum();
        } 

        //跳到下一頁
        StopAudioPlayer();
        DisableRecorder();
        StartCoroutine(PostQuestion());
       

    }


    //clear the drawing



    //save the drawing
    public void Btn_SaveDrawing()
    {
        Answered = true;
        DrawingHolderGroup.blocksRaycasts = false;

        Sprite spr = darwImage.GetComponent<Image>().sprite;
        if (QuestionList[CurrentQuestion].Index != "") {
            string filename = QuestionList[CurrentQuestion].Index + "_" + userId +"_"+GetTimeStamp()+".png";
            //保存到數據庫
            endTimeStamp = GetTimeStamp();
            myWWW.SaveToDatabase(userId, int.Parse(QuestionList[CurrentQuestion].Index), filename, startTimeStamp, endTimeStamp);
            Debug.Log("finlename:" + filename);
            //把圖片保存到服務器
            myWWW.UploadMedia(1, filename, spr);
            UpdateQuestionNum();
        }   

        StopAudioPlayer();
        StartCoroutine(PostQuestion());
        

    }

    public void ShowRewardedVideo()
    {
#if USE_ADMOB
        AdmobManager.instance.ShowRewardedAd();
#endif
    }

#if USE_ADMOB
    internal void IncreaseLives(int increase = 1)
    {
        ExtraLifeButton.SetActive(false);
        RewardedShown = true;

        LivesLeft += increase;
        UpdateLives();
    }
#endif

    public void Continue()
    {
        PostQuestionButtons.SetActive(false);
        StopAudioPlayer();

        if (EnableLives && LivesLeft <= 0)
        {
            GameOver();
            return;
        }

        if (CurrentQuestion < QuestionsLimit)
        {

#if USE_DOTWEEN
            ResetScale();
#endif
            CurrentQuestion++;
            StartCoroutine(LoadQuestion());
        }
        else
        {
            string str = hasFinished;
            if (str == "")
            {
                str = QuestionList[CurrentQuestion].TextIndex.ToString();
            }
            else {
                str = str + "," + QuestionList[CurrentQuestion].TextIndex.ToString();
            }

            hasFinished = str;
            PlayerPrefs.SetString("hasFinished", str);
            
            //保存到數據庫
            StartCoroutine(myWWW.UpdateUser());
            GameOver();
        }
            
    }

    internal void GameOver()
    {
        isGameOver = true;
        isInGame = false;
        GameOverCanvas.SetActive(true);

#if USE_ADMOB
        TotalGameOvers++;

        if (TotalGameOvers >= AdmobManager.instance.ShowInterstitialAdAfterXGameovers)
        {
            AdmobManager.instance.ShowInterstitialAd();
            TotalGameOvers = 0;
        }
#endif

       /* if (!ShowPostQuestionButtons)
            Resources.UnloadUnusedAssets();

        int CategoryHighscore = PlayerPrefs.GetInt(CurrentCategory.HighscorePref, 0);

        if (CurrentScore > CategoryHighscore)
        {
            CategoryHighscore = CurrentScore;
            PlayerPrefs.SetInt(CurrentCategory.HighscorePref, CurrentScore);
            PlayerPrefs.Save();
        }*/

        CategoryImage.sprite = CurrentCategory.CategoryImage;
        CategoryNameText.text = CurrentCategory.CategoryName;

        //GameScoreText.text = CurrentScore.ToString();
       // HighscoreText.text = string.Concat("Highscore: ", CategoryHighscore);
    }

    public void PauseGame()
    {
        if (WaitingForImage)
            return;

        if (!EnablePausing)
        {
            QuitGamePanel.SetActive(true);
            return;
        }
       

        Paused = true;
        PauseAudioPlayer();
        PauseCanvas.SetActive(true);
        float progress = (QuestionLeft + 0.01f) / (TotalQuestionNum + 0.01f);
        Contenttxt.text = "預計還有" + Math.Ceiling(progress * CurrentCategory.Time2Finishe/60) + "分鐘就能完成測試了，如果現在退出測試的話，會失去前面測試的數據，確定要退出嗎？";
    }

    public void ResumeGame()
    {
        if (!EnablePausing)
        {
            QuitGamePanel.SetActive(false);
            return;
        }

        Paused = false;
        PauseCanvas.SetActive(false);
        RestartAudioPlayer();
        StartCoroutine(StartTimer());
    }

    public void RestartGame()
    {
        if (isHybrid && CurrentCategory.Mode == DownloadMode.Offline)
        {
            CurrentCategory.Mode = DownloadMode.Hybrid;
            LoadCategory(CurrentCategory);
            return;
        }

        PrepareGame();
    }

    //截取内容
    public IEnumerator CaptureByUI(RectTransform UIRect, string mFileName = "tempCapture.png")
    {
        //等待幀畫面渲染結束
        yield return new WaitForEndOfFrame();
        
        int width = (int)(UIRect.rect.width);
        int height = (int)(UIRect.rect.height);
        Debug.Log("width:" + width + ";height:" + height);

        Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);

        //左下角爲原點（0, 0）
        float leftBtmX = UIRect.transform.position.x + UIRect.rect.xMin;
        float leftBtmY = UIRect.transform.position.y + UIRect.rect.yMin;

        Debug.Log("leftBtmX:" + leftBtmX + ";leftBtmY:" + leftBtmY);

        //從屏幕讀取像素, leftBtmX/leftBtnY 是讀取的初始位置,width、height是讀取像素的寬度和高度
        tex.ReadPixels(new Rect(leftBtmX, leftBtmY, width, height), 0, 0);
        //執行讀取操作
        tex.Apply();
        byte[] bytes = tex.EncodeToPNG();
        //保存
        System.IO.File.WriteAllBytes(Application.dataPath + "/Resources/tempImg/" + mFileName, bytes);
    }

}