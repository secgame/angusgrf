﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VideoController : MonoBehaviour
{

    private VideoPlayer video;
    private float videoFrame;
    void Awake()
    {
        video = GetComponent<VideoPlayer>();
    }

    void Update() {
        VideoEnd();
    }

    //方法一
    void VideoEnd()
    {
        //video.frame代表的当前帧数；
        //video.frameCount代表视频总帧数；
        videoFrame = video.frame;
        if (videoFrame >= video.frameCount)
        {
            //TODO视频播放完毕后的逻辑
            SceneManager.LoadScene("Main");
        }
    }

    public void btnClose() {
        SceneManager.LoadScene("Main");
    }

}